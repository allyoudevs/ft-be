<?php
	session_start();
  	include ("./inc/connessione.inc.php");
	include ("./inc/ins_del_add_chg.php");
	include ("./inc/Funzioni.php");
	VerificaPrivilegi($_SESSION["Reparto"],"All",$_SESSION["Loggato"],"");

?>
<?php		
	include("./inc/body_top.inc.php");
$modifica= 1;
$visualizza = 1;
$cancella = 1;
$chiudi = 0;
$popup = 0;
$AddPerMail = 0;
$SubjectMailAdd = "";
$MailAdd = "";
$ContenutoMail = ""; /* HTML - TEXT*/
$MailFromAdd = "";
$rientro = "";
$PathUpload = "attach";
$LabelSubmit = "Invia";
$add = 1;
$MultiComando = 1;

$pagmenu="control.php";
$pagina="user.php";
$kiaveSecondaria="";
$Esterni="users2logs.php,invia_iscrizione.php";
$EsterniNome="Log,Invia Iscrizione";
$EsterniIcone = "glyphicon glyphicon-th-large,glyphicon glyphicon-log-in,glyphicon glyphicon-envelope";
$recordsupag="30";

$campoId="id";
$tabella="users";

$campi="nome_cognome,user,pass,email,note";
$nomeCampi="Cognome e Nome,user,pass,email,note";
$tipoCampi=",";

$labelModifica="idCliente,Cognome e Nome,Username,Password,email,stanze,acc. differita,acc. diretta,Relatore,note";
$CampiModifica="idCliente,nome_cognome,user,pass,email,corsi,differita,diretta,admin,note";
$LarghezzaCampi = ",10,10,10,10,10,2,2,2,10";
$TipoCampi="hidden,48,48,48,48,checkbox[rooms where id>0 and idCliente=".$_SESSION["utente"]["cliente"]."++id++name],select[NO++NO++SI++SI],select[NO++NO++SI++SI],select[NO++NO++SI++SI],48area12";
$MaxSpace = ",,";
$ValDefault=$_SESSION["utente"]["cliente"] . "++++++++++++NO++NO++NO";

$LabelSearch = "Testo,Valori Per Pagina,Tipo Accesso";
$CampiSearch = "Testo,ValoriPerPagina,TipoAccesso";
$TipoCampiSearch  = "Testo,Testo,select[diretta++diretta++differita++differita]";
$TipoClausola = "TextLike,TextLike,TextLike";
$CollegamentoClausole = "AND++AND++AND";

if (strlen(trim($_REQUEST["Testo"]))>0) {
	$filtroTestuale = " and (user like '%".$_REQUEST["Testo"]."%' or email like '%".$_REQUEST["Testo"]."%' or note like '%".$_REQUEST["Testo"]."%')";
}

if (preg_match("/^[0-9]{1,10}$/i",$_REQUEST["ValoriPerPagina"])) {
	$recordsupag = $_REQUEST["ValoriPerPagina"];
}

if (preg_match("/^(diretta|differita)$/i",$_REQUEST["TipoAccesso"])) {
        $filtro_accesso = " and ".$_REQUEST["TipoAccesso"]." = 'SI' ";
}

$filtro=" 1=1 " . $filtroTestuale . $filtro_tipo . $filtro_accesso . " and idCliente = " .$_SESSION["utente"]["cliente"];
$ordine="";

$Action=$_GET["Action"];
if ($Action=="") {
$Action=$_POST["Action"];
}

$fai=$_POST["fai"];
if ($fai=="") {
$fai=$_GET["fai"];
}

$id=$_GET["IdRiferimento"];
if ($id=="") {
$id=$_POST["IdRiferimento"];
}

$Sec=$_GET["secondary"];
if ($Sec=="") {
$Sec=$_POST["secondary"];
}

$Ord=$_GET["Ordine"];

if (!empty($Ord)) {
$ordine=$_GET["Ordine"] ;
}

$PathFoto = $PathBase . $PathUpload;

include ("./inc/switch.php");
?>
<script language="javascript">

function doit(campo) {
	var i = 0;
	var stringa = "idop=";
	var p=[];
	var g = "";
	$('.indrig').each( function() {
		if ($(this).prop('checked')) {
			stringa = stringa + g + $(this).val();
			i = i + 1;
			g = ",";
		}
	} );
	if (campo == 1) {
		Modifica("PreparaMail.php?" + stringa);
	}
	if (campo == 2) {
		Modifica("IscriviStudenti.php?" + stringa);
	}	
        if (campo == 3) {
                Modifica("CancellaStudenti.php?" + stringa);
        }
}
</script>
<?php
	include("./inc/footer.inc.php");
?>
