body {
margin: 0px;
padding: 0px;
text-align: center;
font-family: Verdana, sans-serif;
font-size: 11px;
color: #333;
background: #FFFFFF url('../img/bgBody.gif') repeat-x 0px 0px;
}

a {
color: #73829e;
text-decoration: none;
}
		
a:hover {
color: #333;	
}

table {
margin: 10px auto;
width: 99%;
border-bottom: 1px solid #73829E;
}

table td {
padding: 1px;
font-family: Verdana, sans-serif;
font-size: 11px;
border-top: 1px solid #cccccc;
border-left: 1px solid #cccccc;
}

table td a {
width: 100%;
height: 100%;
display: block;
}

table td a.ppp {
width: 100%;
height: 100%;
display: inline;
}

table td a:hover {
background-color: #fff;
background-position: -18px 50%;
}

	.tTitolo {
	background: #fff;
	border-top: 1px solid #cccccc;
	border-bottom: 3px solid #73829E;
	}

	.bTitolo {
	background: #fff;
	border-top: 1px solid #cccccc;
	border-bottom: 3px solid #73829E;
	font-size: 12px;
	text-align: center;
	}
	
	.Click {
	border-left: 1px solid #73829E;
	border-top: 1px solid #73829E;
	}
	
	.Show {	
	line-height: 20px;
	font-size: 10px;
	text-align: right;
	}
	
	.Change {
	line-height: 20px;
	font-size: 10px;
	text-align: right;
	}

	.Modify {
	line-height: 20px;
	font-size: 10px;
	text-align: right;	
	}
	
	.Delete {
	line-height: 20px;
	font-size: 10px;
	text-align: right;
	background: url('../img/Delete.gif') no-repeat -15px 50%;
	}
	
	.Tab {
	line-height: 20px;
	font-size: 10px;
	text-align: right;
	}
	
ul {
list-style: none;
margin: 0px;
padding: 0px;
}

#wrapper {
margin: 0px auto;
width: 984px;
}

	#menuTop {
	text-align: left;
	position: relative;
	margin: 0px auto;
	width: 984px;
	height: 30px;
	color: #e1e1e1;
	}
	
		#menuTop span {
		position: absolute;
		top: 10px;
		left: 0px;
		height: 25px;
		line-height: 8px;
		}
	
		#menuTop a {
		font-size: 10px;
		color: #73829e;	
		text-decoration: none;
		}
		
		#menuTop a:hover {
		color: #e1e1e1;	
		}
	
		#menuTop ul {
		position: absolute;
		top: 5px;
		right: 0px;
		margin: 0px;
		padding: 0px;
		height: 25px;
		}
		
		#menuTop ul li {
		position: relative;
		float: left;
		width: 80px;
		margin: 0px 0px 0px 3px;
		padding: 0px;
		height: 25px;		
		background: url('../img/angolosxliMenuTop.gif') no-repeat 0px 0px;
		}
		
		#menuTop ul li a {
		display: block;
		height: 25px;
		margin: 0px 0px 0px 5px;
		padding: 0px 10px 0px 5px;
		background: url('../img/bgliMenuTop.gif') repeat-x 0px 0px;
		line-height: 23px;
		font-size: 11px;
		color: #999;
		text-align: center;
		text-decoration: none;
		}
		
			.lineaHover {
			z-index: 100;
			display: none;
			position: absolute;
			left: 0px;
			bottom: -5px;
			width: 80px;
			height: 5px;
			background: #73829e;
			}
		
		#menuTop ul li a:hover {
		color: #e1e1e1;
		}
		
			#menuTop ul li a:hover .lineaHover {
			display: block;
			}
	
	#header {
	position: relative;
	margin: 0px auto;
	width: 980px !important;
	width: 984px;
	height: 99px;
	border-left: 1px solid #e1e1e1;
	border-right: 1px solid #e1e1e1;
	background: #fff center;
	}
	
		#header .wellcome {
		position: absolute;
		right: 10px;
		top: 10px;
		margin: 0px;
		padding: 0px;
		text-align: right;
		color: #999;
		}
	
	
#content {
text-align: left;
border-left: 1px solid #c8c8c8;
border-right: 1px solid #c8c8c8;
height: 80%;
}

#boxIndex {
position: relative;
width: 100%;
float: left;
}

	#boxIndex .left {
	position: absolute;
	top: 0px;
	left: 0px;
	}
	
	#boxIndex .middle {
	position: absolute;
	top: 0px;
	left: 328px;
	}
	
	#boxIndex .right {
	position: absolute;
	top: 0px;
	right: 0px;
	}
	
		#content ul {
		width: 324px;
		margin: 10px 0px;
		padding: 0px;
		}
		
		#content li {
		margin: 0px;
		padding: 0px 0px 0px 15px;	
		border-bottom: 1px dotted #e1e1e1;
		background: #F0F0EC;
		}
		
		#content li a {
		display: block;
		width: 100%;
		height: 18px;
		font-size: 11px;
		line-height: 18px;
		}
		
		#content li a:hover {
		background: #fff url('../img/frecciaLi.gif') no-repeat right top;
		}
		
		#content .titolo {
		margin: 0px 0px;
		padding-left: 10px;
		background: none;
		height: 22px;
		border-right: none;
		border-bottom: none;
		}
		
		#content h3 {
		margin: 0px;
		padding: 0px;
		font-size: 14px;	
		font-family: Arial, sans-serif;
		line-height: 14px;
		}

.dettsec {
	font-size: 14px;
	color: red;
}

td.GiornoFeriale {
	text-align: center;
}

td.GiornoFestivo {
	text-align: center;
	color: red;
}

td.Calprenotazione {
	background-color : orange;
	text-align: center
}

td.CalNoleggio {
	background-color : red;
	text-align: center
}

td.CalConcluso {
	background-color : red;
	text-align: center
}

td.CalNo {
	background-color : #FFF99D;
	text-align: center
}

td.Cal {
	background-color : #39F100;
	text-align: center
}
	
a.neutra {
	display : inline;
}

td.CRT {
	font-size: 12px;
}

//menu a tendina

#csstopmenu, #csstopmenu ul{
padding: 0;
margin: 0;
list-style: none;
}

#csstopmenu li{
float: left;
position: relative;
}

#csstopmenu a{
text-decoration: none;
}

.headerlinks a{
margin: auto 8px;
font-weight: bold;
color: black;
}

.submenus{
display: none;
width: 17em;
position: absolute;
top: 1.2em;
left: 0;
background-color: #F0F0EC;
border: 1px solid black;
z-index: 10;
}

.submenus li{
width: 100%;
z-index: 10;
}

.submenus li a{
display: block;
width: 100%;
text-indent: 3px;
z-index: 10;
}

html>body .submenus li a{ /* non IE browsers */
width: auto;
z-index: 10;
}

.submenus li a:hover{
background-color: yellow;
color: black;
z-index: 10;
}

#csstopmenu li>ul {/* non IE browsers */
top: auto;
left: auto;
z-index: 10;
}

#csstopmenu li:hover ul, li.over ul {
display: block;
z-index: 10;
}

html>body #clearmenu{ /* non IE browsers */
height: 3px;
z-index: 10;
}

a.Bottone:link, a.Bottone:visited {
  display: inline;
  width: 250px;
  padding: 0.2em;
  line-height: 1.4;
  background-color: #94B8E9;
  border: 1px solid black;
  color: #000;
  text-decoration: none;
  text-align: center;
}

a.Bottone:hover {
 background-color: #369;
 color: #fff;
}

	
/*******************/
	<?php include("../../inc/connessione.inc.php");
			$Sql = "Select * from AdminCategorie order by Ordine asc";
			$result = mysql_query($Sql,$conn);
			while ($row=mysql_fetch_array($result)) {
				echo ".Ctg".$row["IdAdmCtg"]." .titolo {border-left: 5px solid ".$row["Colore"].";}";
				echo ".Ctg".$row["IdAdmCtg"]." h3 {color: ".$row["Colore"].";}";
				echo ".Ctg".$row["IdAdmCtg"]." li {border-left: 1px solid ".$row["Colore"].";}";
				echo ".Ctg".$row["IdAdmCtg"]." li {border-left: 1px solid ".$row["Colore"].";}";
				echo ".mainitems".$row["IdAdmCtg"]." {border: 1px solid black; border-left-width: 0; background-color: #E5E6DF; width: 161px; color: ".$row["Colore"]."; font-size: 14px;}";
			}
	?>
