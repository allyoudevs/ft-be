<?php
	session_start();

  	include ("./inc/connessione.inc.php");
	//COME PRIMA COSA FACCIO UNA VERIFICA CHE CI SIANO I PERMESSI DI CHIUSURA
if ( $_SESSION["admin"] == 1) {	
	require_once('../api/bbb-api.php');

	$testo = "STANZA CHIUSA CON SUCCESSO, INIZIO LA GENERAZIONE DELLA DIFFERITA";

	// Instatiate the BBB class:
	$bbb = new BigBlueButton();
	$itsAllGood = true;
	try {
		$result = $bbb->isMeetingRunningWithXmlResponseArray($_REQUEST["id"]);
		//print_r($result);
		$tmp_aaa = xml2array($result["running"]);
		if ($tmp_aaa["running"] == 'true') {	
			$endParams = array(
                        	'meetingId' => $_REQUEST["id"],
                        	'password' => "mp"
                	);
                	try {
				$result = $bbb->endMeetingWithXmlResponseArray($endParams);
				//print_r($result);
				$testo = "DIRETTA CHIUSA";
			} catch (Exception $e) {
                                echo 'Caught exception: ', $e->getMessage(), "\n";
                                $itsAllGood = false;
				$testo = "ERRORE CHIUSURA DIRETTA, SE l'ERRORE PERSISTE CONTATTARE IL SUPPORTO"; 
               	}	
		}
	} catch (Exception $e) {
		//echo 'Caught exception: ', $e->getMessage(), "\n";
		$itsAllGood = false;
		$testo = "ERRORE CHIUSURA DIRETTA, SE l'ERRORE PERSISTE CONTATTARE IL SUPPORTO";
	}
	
	echo $testo;
} else {
	echo "NON HAI I PERMESSI PER CHIUDERE LA STANZA";1
}
?>
