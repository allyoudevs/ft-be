<?php
        session_start();

        include ("./inc/connessione.inc.php");
        include("./inc/body_top.inc.php");
        include("./inc/Funzioni.php");
	$loader = require_once __DIR__.'/vendor/autoload.php';

	        use BigBlueButton\BigBlueButton;
                use BigBlueButton\Parameters\GetRecordingsParameters;

                VerificaPrivilegi($_SESSION["Reparto"],"All",$_SESSION["Loggato"],"");
                if ($_REQUEST["Mod"]=='Ok') {
                echo "<script language=javascript>confirm('Modifica inserita correttamente nel sistema');</script>";
                }
?>

                    <h1>Registrazioni</h1>



                    <div class="well center-block">    

                    	<a href="javascript:Modifica('chiudi_diretta.php?id=<?= $_REQUEST["secondary"]?>')" class="btn btn-primary btn-lg btn-block" style="line-height: 2em;"><span class="glyphicon glyphicon-record"></span> Chiudi diretta</a>

                   </div> 

<?php
	$sql = "select * from rooms where id =".$_REQUEST["secondary"].";";
	$risultato = mysqli_query($conni,$sql);
	if ($riga = mysqli_fetch_array($risultato)) {

		$recordingParams = new GetRecordingsParameters();
		$recordingParams->setMeetingId($_REQUEST["secondary"]);
		$bbb = new BigBlueButton();
		$response = $bbb->getRecordings($recordingParams);

		if ($response->getReturnCode() == 'SUCCESS') {
			foreach ($response->getRawXml()->recordings->recording as $recording) {

//			print_r($recording->playback->format);
?>
                    	<br>
                    	<p class="lead text-center">Registrazione iniziata il <strong><?= date("d/m/Y",ceil($recording->startTime/1000))?></strong> alle ore <?= date("H:i",ceil($recording->startTime/1000)) ?>
                    	<br>Registrazione terminata il <strong><?= date("d/m/Y",ceil($recording->endTime/1000)) ?></strong> alle ore <?= date("H:i",ceil($recording->endTime/1000)) ?></p>
                    	<br>
                        <div class="well">
                      	<div class="row">

                        	<div class="col-md-6">

                          		<a class="btn btn-default btn-block" style="line-height: 2em;" href="<?= $recording->playback->format->url?>" target="new"><span class="glyphicon glyphicon-play"></span>Differita</a>
                        	</div>
                        	<div class="col-md-6">
                          		<a class="btn btn-default btn-block" style="line-height: 2em;" href="<?= $rcanc ?>"><span class="glyphicon glyphicon-remove"></span> Cancella</a>
                        	</div>
                        </div>
                </div>
<?	
			}
		}
	}
include ("./inc/footer.inc.php");
?>
