<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Auviol</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<script src="js/script.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav" id="side-menu">
            <li class="sidebar-brand"><img src="image/logo-auviol.png" class="img-responsive" /></li>
            <li> <a href="control.php"><span class="glyphicon glyphicon-dashboard"></span> Cruscotto</a> </li>
<?php
		$StringaLink = base64_encode($_SERVER["REQUEST_URI"]);
		$sql = "select * from pagine order by ordine asc";
		$risultato = mysqli_query($conni,$sql);
		while ($riga = mysqli_fetch_array($risultato)) {
			if ( ( $riga["permesso"] == 'admin' and $_SESSION["utente"]["livello"] == 'admin') or ( $riga["permesso"] == 'cliente' and $_SESSION["utente"]["cliente"] >0) ) {
?>
            <li> <a href="#"><span class="<?= $riga["classe"] ?>"></span> <?= $riga["nomePagina"] ?></a>
                <ul class="nav nav-second-level collapse">
                    <li> <a href="<?= $riga["pagina"] ?>"><?= $riga["testoElenco"] ?></a> </li>
                    <li> <a href="<?= $riga["pagina"] ?>?fai=3&ritorna=<?= $StringaLink?>"><?= $riga["testoInserimento"] ?></a> </li>
		    <?php
			if ($riga["pagina"] == 'user.php') {
?>
			<li> <a href="importa_utenti.php">Importa</a> </li>
<?php
			}
		    ?>
                </ul>
                <!-- /.nav-second-level -->
            </li>
<?php
			}
	   }
?>
            <li> <a href="index.php"><span class="glyphicon glyphicon-log-out"></span> Log Out</a> </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

