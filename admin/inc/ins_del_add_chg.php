<?php
function show($conni,$tabella,$campi,$nomeCampi,$tipoCampi,$campoId,$pagina,$filtro,$ordine,$kiaveSecondaria,$Sec,$recordsupag,$pagmenu,$fuori,$EsterniNome,$EsterniIcone,$modifica,$visualizza,$cancella,$popup,$FotoPathAb,$FotoPathRel,$IdPerIndietro,$VengoDa,$LabelSearch,$CampiSearch,$TipoCampiSearch,$TipoClausola,$CollegamentoClausole,$StringaCerca,$CampoCancella,$add,$MultiComando) {

$StringaLink = base64_encode($_SERVER["REQUEST_URI"]);

$sql = "select * from pagine where pagina = '".$pagina."'";
$risultato = mysqli_query($conni,$sql);
if ($riga = mysqli_fetch_array($risultato)) {
	$TitoloPagina = $riga["nomePagina"];
}

global $LabelDettagli;

$NomiCampi=explode(",", $nomeCampi);
$CampiPrint=explode(",",$campi);
$vettTipiCampi=explode(",",$tipoCampi);
$ListaCampiCerca=explode(",",$CampiSearch);

if (file_exists(str_replace(".php","Form.php",$pagina))) {
	$PaginaModifica = str_replace(".php","Form.php",$pagina);
} else {
	$PaginaModifica = $pagina;
}

$CalusolaUlterioreLink = "";
	for ($i=0;$i<count($ListaCampiCerca);$i++) {
		$CalusolaUlterioreLink .= $ListaCampiCerca[$i] . "=" . $_REQUEST[$ListaCampiCerca[$i]] . "&";
	}
if ($fuori<>"") {
$VettEst=explode(",",$fuori);
$VettNomiEst = explode(",",$EsterniNome);
$VettIcoEst = explode(",",$EsterniIcone);
}
if ($_REQUEST["Pag"]!="") {
$PagCorrente=$_REQUEST["Pag"];
} else {
$PagCorrente=1;
}
?>
<h1>Elenco <?= $TitoloPagina ?></h1>
<?php
CreaMotoreRicerca($conni,$tabella,$CampiSearch,$TipoCampiSearch,$campoId,$id,$LabelSearch,$pagina,"Cerca");
echo "<table class=\"table table-striped table-condensed table-hover\">";
if ($MultiComando == 1) {
                                echo '<th >&nbsp;</td>';
                        }
for ($x=0;$x<count($NomiCampi);$x++){
		if ($CampiPrint[$x] . " ASC" == $_REQUEST["Ordine"]) {
			$PerOrdinamento = $CampiPrint[$x] . " DESC";
		} else {
			$PerOrdinamento = $CampiPrint[$x] . " ASC";
		}
	echo '<th align=center ><a href=" ' . $pagina . '?Ordine=' . $PerOrdinamento  . '&Pag=' . $PagCorrente . '&secondary='.$_REQUEST["secondary"].''.$StringaCerca.'&'.$CalusolaUlterioreLink.'">'.$NomiCampi[$x].'</a></td>';
}
echo '</tr>';
if ($kiaveSecondaria!="") {
$condizione = " AND ".$kiaveSecondaria."=".$Sec." ";
}
if ($ordine!="") {
$order= "Order by ".$ordine."";
}
//Cerco se C'� un campo di nome conta su db... che per me coincide con un count(*)
if (ereg("^conta",$campi)) {
$campi = str_replace(",Minim,Maxim,Attivo","",$campi);
$sql='Select Count(*) as '.$campi.',DATE_FORMAT(MAX(Data),\'%d/%m/%Y\') as Maxim,DATE_FORMAT(MIN(Data),\'%d/%m/%Y\') as Minim,IF(TipoScheda>0 and DataScadenza>NOW(),\'SI\',\'NO\') as Attivo From '.$tabella.' where '.$filtro.' '.$condizione.' '.$order.';';
} else {
$sql='Select * From '.$tabella.' where '.$filtro.' '.$condizione.'  '.$order.';';
}

//echo $sql;
$result=mysqli_query($conni,$sql);
$punto=1;
$NumeroRisultati = mysqli_num_rows($result);
	while ($row = mysqli_fetch_array($result)) {
		if ($punto>=((($PagCorrente-1)*$recordsupag)+1) & $punto<=($PagCorrente*$recordsupag)) {
			echo '<tr>';
			if ($MultiComando == 1) {
                                echo '<td><input type="checkbox" name="IDRIGA[]" id="IDRIGA[]" class="indrig" value="'.$row[$campoId].'"></td>';
                                }
			for ($x=0;$x<count($NomiCampi);$x++){
				if ($vettTipiCampi[$x]=="img") {
					printf ('<td align=center ><a href="javascript:FotoPopUp(\'upload.php?tab=%s&campoid=%s&id=%s&campo=%s&imma=%s&path=%s\')"><img src="../%s/%s" border=0 width=40 height=40></a></td>',$tabella,$campoId,$row[$campoId],$CampiPrint[$x],$row[$CampiPrint[$x]],$FotoPathAb,$FotoPathRel,$row[$CampiPrint[$x]]);
					} else if (ereg("^allegato", $vettTipiCampi[$x])) {
                                                if ($row[$CampiPrint[$x]]=="") {
                                                        printf ('<td align=center ><nobr>Allegato</nobr><br><a href="javascript:FotoPopUp(\'upload.php?tab=%s&campoid=%s&id=%s&campo=%s&imma=%s&path=%s&nomefile=%s&file=%s\')">Inserisci</a></td>',$tabella,$campoId,$row[$campoId],$CampiPrint[$x],$row[$CampiPrint[$x]],$FotoPathAb,$CampiPrint[$x],$row[$CampiPrint[$x]]);
                                                } else {
                                                        printf ('<td align=center ><nobr>Allegato</nobr><br><a href="javascript:FotoPopUp(\'upload.php?tab=%s&campoid=%s&id=%s&campo=%s&imma=%s&path=%s&nomefile=%s&file=%s\')" class="ppp">Modifica</a>-<a href="%s/%s" target="new" class="ppp">Mostra</a></td>',$tabella,$campoId,$row[$campoId],$CampiPrint[$x],$row[$CampiPrint[$x]],$FotoPathAb,$CampiPrint[$x],$row[$CampiPrint[$x]],$FotoPathRel,$row[$CampiPrint[$x]]);
						}
					} else if (ereg("^find", $vettTipiCampi[$x])) {
						$Variabile=substr($vettTipiCampi[$x], 5, strlen($vettTipiCampi[$x])-6);
						echo "<td>";
						echo FindDati($Variabile,$row[$CampiPrint[$x]]);
						echo "</td>";
					} else if (ereg("^funct", $vettTipiCampi[$x])) {
						$Variabile=substr($vettTipiCampi[$x], 6, strlen($vettTipiCampi[$x])-7);
						$Variabile = str_replace("|",",",$Variabile);
						echo "<td align=\"right\">";
						echo eval($Variabile.";");
						echo "</td>";
					} else if (ereg("^Valuta", $vettTipiCampi[$x])) {
                    	echo '<td >� '.comma_format($row[$CampiPrint[$x]]).'&nbsp;</td>';
					} else if (ereg("^Date", $vettTipiCampi[$x])) {
                    	echo '<td >'.MyHuman($row[$CampiPrint[$x]]).'&nbsp;</td>';
					} else if (ereg("^DatAndTime", $vettTipiCampi[$x])) {
                        echo '<td >'.MyHumanConOra($row[$CampiPrint[$x]]).'&nbsp;</td>';
                                        }else if (ereg("^sfondo", $vettTipiCampi[$x])) {
					$Variabile=substr($vettTipiCampi[$x], 7, strlen($vettTipiCampi[$x])-8);
					$ColoreSfondo="bgcolor=\"none\"";
					$ListaColori=explode("++",$Variabile);
						for ($s=0;$s<=count($ListaColori);$s=$s+2) {
							if ($row[$CampiPrint[$x]] == $ListaColori[$s] ) {
								$ColoreSfondo="bgcolor=\"".$ListaColori[$s+1]."\"";
							}
						}
					echo '<td  '.$ColoreSfondo.'>'.$row[$CampiPrint[$x]].'&nbsp;</td>';
					} else {
					echo '<td >'.utf8_encode($row[$CampiPrint[$x]]).'&nbsp;</td>';
					}
				echo "\n";
			}
			if (file_exists(str_replace(".php","Mostra.php",$pagina))) {
				$PaginaVedi = str_replace(".php","Mostra.php",$pagina);
			} else {
				$PaginaVedi = $pagina;
			}
			if ($visualizza==1) {
				if ($popup==1) {
						printf ('<td><a href="javascript:Visualizza(\'%s?fai=5&IdRiferimento=%s\')" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Dettagli"><span class="glyphicon glyphicon-eye-open"></span></a></td>',$PaginaVedi,$row[$campoId]);
				} else {
						printf ('<td><a href="%s?fai=5&IdRiferimento=%s" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Dettagli"><span class="glyphicon glyphicon-eye-open"></span></a></td></td>',$PaginaVedi,$row[$campoId]);				
				}
			}
			
			if ($modifica==1) {
				if ($popup==1) {
						printf ('<td><a href="javascript:Modifica(\'%s?fai=2&IdRiferimento=%s&ritorna=%s\')" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Modifica"><span class="glyphicon glyphicon-pencil"></span></a></td>',$PaginaModifica,$row[$campoId],$StringaLink);
				} else {
						printf ('<td><a href="%s?fai=2&IdRiferimento=%s&ritorna=%s" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Modifica"><span class="glyphicon glyphicon-pencil"></span></a></td>',$PaginaModifica,$row[$campoId],$StringaLink);
				}
			}
			if ($cancella==1) {
				if ($popup==1) {
						printf ('<td><a href="javascript:Cancella(\'%s?fai=4&IdRiferimento=%s&Action=del\',\'%s\',1)" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Cancella"><span class="glyphicon glyphicon-remove"></span></a></td>',$pagina,$row[$campoId],$row[$CampoCancella]);
				} else {
					printf ('<td><a href="javascript:Cancella(\'%s?fai=4&IdRiferimento=%s&Action=del&ritorna=%s\',\'%s\',0)" class="btn btn-default btn-sm" role="button" data-toggle="tooltip" title="Cancella"><span class="glyphicon glyphicon-remove"></span></a></td>',$pagina,$row[$campoId],$StringaLink,$row[$CampoCancella]);
				}

			}
			if ($fuori<>"" OR is_array($VettEst)) {
				for ($x=0;$x<count($VettEst);$x++){
					 if (preg_match("/{/i",$VettEst[$x])>0) {
 						$LinkRelazione = ReplaceStringaInMezzo($VettEst[$x],"{","}",$row);
 					 } else {
 						$LinkRelazione = $VettEst[$x];
					 }
					 	if ($VengoDa!="") {
							$PaginaRiferimentoPerRitorno = $VengoDa. "," . $pagina;
							$IdRiferimentoPerIndietro = $IdPerIndietro . "," . $Sec;
						} else {
							$PaginaRiferimentoPerRitorno = $pagina;
							$IdRiferimentoPerIndietro = $Sec;
						}

					if (file_exists($LinkRelazione)) {
						echo "<td><a href=\"" . $LinkRelazione . "?secondary=" . $row[$campoId] . "&back=".$IdRiferimentoPerIndietro."&PagSource=".$PaginaRiferimentoPerRitorno."\" class=\"btn btn-default btn-sm\" role=\"button\" data-toggle=\"tooltip\"  title=\"".$VettNomiEst[$x]."\"><span class=\"".$VettIcoEst[$x]."\"></span></a></td>";
					}
				}
			}
		echo "</tr>";
		}
	$punto = $punto + 1;

	}
if ($MultiComando == 1) {
	echo "<tr>
		<td><input name=\"seldes\" id=\"seldes\" type=\"checkbox\" value=\"1\" onchange=\"Spunta();\"></td>
		<td colspan=\"6\"><em>&larr; Seleziona/Deseleziona tutti</em></td>
		</tr>";
}

echo '</table>';

echo "                 <div class=\"row\">
                    	<div class=\"col-md-6\"> ";             
if ($MultiComando == 1) {        
echo "                    <em>Se selezionati:</em>";
	$sql = "select * from AdminMulticomando where Pagina = '".$pagina."'";
	$risultato = mysqli_query($conni,$sql);
	while ($riga = mysqli_fetch_array($risultato)) {
        	echo "                  <a class=\"btn btn-primary\" href=\"#\" role=\"button\" onclick=\"".$riga["Azione"]."\"> ".$riga["NomeComando"]." </a>";
	}
}
echo "                </div>
                       <div class=\"col-md-6\">                        
                          <ul class=\"pagination pull-right\">
                            <li>
                              <a href=\"".$pagina."?Pag=1&Ordine=".$_REQUEST["Ordine"]."&secondary=".$_REQUEST["secondary"]."".$StringaCerca."&".$CalusolaUlterioreLink."\" aria-label=\"Previous\">
                                <span aria-hidden=\"true\">&laquo;</span>
                              </a>
                            </li>";
for ($count=1; $count<=ceil(($NumeroRisultati/$recordsupag)); $count++) {
echo "			    <li><a href=\"".$pagina."?Pag=".$count."&Ordine=".$_REQUEST["Ordine"]."&secondary=".$_REQUEST["secondary"]."".$StringaCerca."&".$CalusolaUlterioreLink."\">".$count."</a></li>";
}
echo"                        <li>
                              <a href=\"".$pagina."?Pag=".ceil(($NumeroRisultati/$recordsupag))."&Ordine=".$_REQUEST["Ordine"]."&secondary=".$_REQUEST["secondary"]."".$StringaCerca."&".$CalusolaUlterioreLink."\" aria-label=\"Next\">
                                <span aria-hidden=\"true\">&raquo;</span>
                              </a>
                            </li>
                          </ul>
                      </div>
			</div>
		";
}


function change($conni,$tabella,$CampiModifica,$TipoCampi,$campoId,$id,$labelModifica,$pagina,$SubmitTxt,$LenMax,$LarghezzaCampi) {
$VettCampiModifica=explode(",", $CampiModifica);
$VettTipoCampo=explode(",", $TipoCampi);
$VettlabelModifica=explode(",", $labelModifica);
$VettLarghezza=explode(",",$LarghezzaCampi);
$VettLenMax=explode(",",$LenMax);

$sql = "select * from pagine where pagina = '".$pagina."'";
$risultato = mysqli_query($conni,$sql);
if ($riga = mysqli_fetch_array($risultato)) {
        $TitoloPagina = $riga["nomePagina"];
}

echo "<h1>Modifica ".$TitoloPagina."</h1>";

echo '<form action="'.$pagina.'" method=get class="form-horizontal" ><input type=hidden name="Action" value="chg"><input type=hidden name="fai" value="4">';
$sql="Select * From ".$tabella." where ".$campoId."=".$id.";";
//echo $sql;
$result=mysqli_query($conni,$sql);
	$Conta = 0;
	while ($row = mysqli_fetch_array($result)) {
		for ($x=0;$x<count($VettCampiModifica);$x++){
			$Conta = CreaPuntoDmmandaForm ($VettTipoCampo[$x],$VettCampiModifica[$x],$row[$VettCampiModifica[$x]],$VettlabelModifica[$x],$VettLenMax[$x],$VettLarghezza[$x],$Conta);
		}
	}
echo "<input type=hidden name=IdRiferimento value=".$id."><input type=hidden name=ritorna value='".$_REQUEST["ritorna"]."'>";
echo "<div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
                <button type=\"submit\" class=\"btn btn-info btn-block btn-lg\">Salva modifiche</button>
        </div>
       </div>
      </form>";
}

function Dettagli($conni,$tabella,$CampiModifica,$TipoCampi,$campoId,$id,$labelModifica,$pagina,$chiudi) {
$XxXx = 1;
$VettCampiModifica=explode(",", $CampiModifica);
$VettTipoCampo=explode(",", $TipoCampi);
$VettlabelModifica=explode(",", $labelModifica);
echo '<table cellspacing=4 cellpadding=4 width="100%"><tr valign=top><td align=center><table><form action="'.$pagina.'" method=post><input type=hidden name="Action" value="chg"><input type=hidden name="fai" value="4">';
$sql="Select * From ".$tabella." where ".$campoId."=".$id.";";
$result=mysqli_query($conni,$sql);
	while ($row = mysqli_fetch_array($result)) {
		for ($x=0;$x<count($VettCampiModifica);$x++){
			echo '<tr valign="top">';
			if (ereg("^label", $VettTipoCampo[$x])) {
			echo '<td colspan=2 align=center><strong>'.$VettlabelModifica[$x].'</strong></td>';
			echo '</tr>';
			} else if (ereg("^SePaRaToRe", $VettTipoCampo[$x])) {
			echo "</table></td><td align=center><table>";
			$XxXx = $XxXx +1;
			} else {
			echo "<td align=right width=\"50%\">".$VettlabelModifica[$x].":</td>";
			CreaDettagli ($VettTipoCampo[$x],$VettCampiModifica[$x],$row[$VettCampiModifica[$x]]);
			echo '</tr>';
			}
		}
	}
echo "</table></td></tr></form>";
if ($chiudi==1) {
echo "<tr><td colspan=".$XxXx." align=center><input type=hidden name=IdRiferimento value=".$id."><a href=\"javascript:window.close();\">Chiudi La Finestra Dettagli</a></td></tr>";
}
echo "</table>";
}

function add($conni,$tabella,$CampiModifica,$TipoCampi,$campoId,$labelModifica,$pagina,$kiaveSecondaria,$Sec,$ValDefault,$SubmitTxt,$LenMax,$LarghezzaCampi) {
if ($ValDefault<>"") {
$VettDefault=explode("++", $ValDefault);
} else {
$VettDefault="vuoto";
}
$VettCampiModifica=explode(",", $CampiModifica);
$VettTipoCampo=explode(",", $TipoCampi);
$VettlabelModifica=explode(",", $labelModifica);
$VettLarghezza=explode(",",$LarghezzaCampi);
$VettLenMax=explode(",",$LenMax);
$Conta = 0;
$sql = "select * from pagine where pagina = '".$pagina."'";
$risultato = mysqli_query($conni,$sql);
if ($riga = mysqli_fetch_array($risultato)) {
        $TitoloPagina = $riga["nomePagina"];
}

echo "<h1>Modifica ".$TitoloPagina."</h1>";

echo '<form action="'.$pagina.'" method=post class="form-horizontal"><input type=hidden name="Action" value="add"><input type=hidden name="fai" value="4">';
		for ($x=0;$x<count($VettCampiModifica);$x++){
			if ($VettDefault<>"vuoto") {
			$Conta = CreaPuntoDmmandaForm ($VettTipoCampo[$x],$VettCampiModifica[$x],$VettDefault[$x],$VettlabelModifica[$x],$VettLenMax[$x],$VettLarghezza[$x],$Conta);
			} else {
			$Conta = CreaPuntoDmmandaForm ($VettTipoCampo[$x],$VettCampiModifica[$x],"",$VettlabelModifica[$x],$VettLenMax[$x],$VettLarghezza[$x],$Conta);
			}
		echo "\n";
		}
echo '<input type=hidden name="secondary" value="'.$Sec.'"><input type=hidden name=ritorna value="'.$_REQUEST["ritorna"].'">';
echo "<div class=\"form-group\">
        <div class=\"col-sm-offset-2 col-sm-10\">
                <button type=\"submit\" class=\"btn btn-info btn-block btn-lg\">Salva modifiche</button>
        </div>
       </div>
      </form>";
}

function execute($conni,$tabella,$CampiModifica,$campoId,$Action,$id,$kiaveSecondaria,$Sec,$CampiLabel,$CampiTipo,$SendMailAdd,$MailAddSbj,$MailAdd,$MailAddFrom,$TipoMail) {
$VettCampiModifica=explode(",", $CampiModifica);
$VettTipoCampo = explode(",",$CampiTipo);
	if ($Action=="add") {
		$sql='INSERT INTO '.$tabella.' (';
		$g="";
		for ($x=0;$x<count($VettCampiModifica);$x++) {
		$sql.=$g."`".$VettCampiModifica[$x]."`";
		$g=",";
		}
		$g="";
		if ($kiaveSecondaria!="") {
		$sql.=','.$kiaveSecondaria.'';
		}
		$sql.=') VALUES (';
		for ($x=0;$x<count($VettCampiModifica);$x++) {
      		  if (ereg("^float",$VettTipoCampo[$x])) {
        		$sql.=$g."'".str_replace(",",".",$_REQUEST[$VettCampiModifica[$x]])."'";
        		} else if (ereg("^Progressivo",$VettTipoCampo[$x])) {
			$sql.=$g."'".NuovoId($conni,$VettCampiModifica[$x],Date("Y"))."'";
                        } else if (preg_match("/^checkbox/", $VettTipoCampo[$x])) {
                        $sql.=$g."'".implode("||",$_REQUEST[$VettCampiModifica[$x]])."'";
			} else if(ereg("^Date",$VettTipoCampo[$x])) {
			$sql.=$g."'".HumanMy($_REQUEST[$VettCampiModifica[$x]])."'";
			} else {
        		$sql.=$g."'".$_REQUEST[$VettCampiModifica[$x]]."'";
        		}
		$g=",";
		}
		if ($kiaveSecondaria!="") {
		$sql.=",'".$Sec."'";
		}
		$sql.=");";
		if ($SendMailAdd==1) {
		
		$TestoMail=CostruttoMail ($CampiModifica,$CampiLabel,$CampiTipo,$TipoMail);
		
		
		$headers  = "MIME-Version: 1.0\n";
		$headers .= "Content-Transfer-Encoding: quoted-printable\n";
		if ($TipoMail == "HTML") {
		$headers .= "Content-type: text/html; charset=\"iso-8859-1\"\n";
		} else {
		$headers .= "Content-type: text/plain; charset=\"iso-8859-1\"\n";
		}
		/* additional headers */
		//$headers .= "To: ".$MailAdd."\r\n";
		$headers .= "Fromanna@pixel-online.net: ".$MailAddFrom."\n";
		/* $headers .= "Cc: birthdayarchive@example.com\r\n";
		$headers .= "Bcc: birthdaycheck@example.com\r\n"; */
		mail($MailAdd,$MailAddSbj,$TestoMail,$headers);
		}	
	}
	if ($Action=="chg") {
		$sql="UPDATE ".$tabella." SET ";
		$g="";
		for ($x=0;$x<count($VettCampiModifica);$x++) {
        if (ereg("float",$VettTipoCampo[$x])) {
        	$sql.=$g."`".$VettCampiModifica[$x]."`='".str_replace(",",".",$_REQUEST[$VettCampiModifica[$x]])."'";
	} else if (preg_match("/^checkbox/", $VettTipoCampo[$x])) {
		$sql.=$g."`".$VettCampiModifica[$x]."`='".implode("||",$_REQUEST[$VettCampiModifica[$x]])."'";
        } else if(ereg("^Date",$VettTipoCampo[$x])) {
			$sql.=$g."`".$VettCampiModifica[$x]."`='".HumanMy($_REQUEST[$VettCampiModifica[$x]])."'";
        } else {
        	$sql.=$g."`".$VettCampiModifica[$x]."`='".$_REQUEST[$VettCampiModifica[$x]]."'";
        }
		$g=",";
		}
		$sql.=" Where ".$campoId."=".$id.";";
	}
	if ($Action=="del") {
	$sql="DELETE FROM ".$tabella." where ".$campoId."=".$id.";";
	}
	/*echo $sql;
	die();*/
	//echo "<br><br><br><b>Salvataggio dati in corso</b>";
	mysqli_query($conni,$sql);
	if ($Action=="add") {
		return mysqli_insert_id();
	}
}

function CreaPuntoDmmandaForm ($tipoCampo,$CampoModifica,$Valore,$labells,$MassimaLunghezza,$Size,$Conta){
			$TestoFinale = "";
			if ($Size > 0) {
				$LarghT = $Size + 2;
				$Largh = $Size;
			} else {
				$LarghT = 10 + 2;
				$Largh = 10;
			}
			if ( ( $Conta + $LarghT ) > 12) {
				echo "</div>";
				$Conta = 0;
			}
			if ($Conta == 0 ) {
				$TestoFinale .= "<div class=\"form-group\">";
			}
                        if ($MassimaLunghezza>0) {
                                $txtAdd = "onblur=\"javascript:VerificaCaratteri(this,".$MassimaLunghezza.")\"";
                        } else {
                                $txtAdd = "";
                        }
                        if (ereg("^selecampo", $tipoCampo)) {
                                $Variabile=substr($tipoCampo, 10, strlen($tipoCampo)-11);
				$TestoFinale .= "<label class=\"col-sm-2 control-label\">".$labells."</label>";
                                $TestoFinale .= "<div class=\"col-sm-".$Largh."\">";
                                $Listato = CreaLista($Variabile);
                                $TestoFinale .= CreaSelect($CampoModifica,$Listato,$Valore,"class=\"form-control\"");
                                $TestoFinale .= "</div>";
                                //echo $Variabile;
                        } else if (preg_match("/^checkbox/", $tipoCampo)) {
                                $Variabile=substr($tipoCampo, 9, strlen($tipoCampo)-10);
                                $TestoFinale .= "<label class=\"col-sm-2 control-label\">".$labells."</label>";
                                $TestoFinale .= "<div class=\"col-sm-".$Largh."\">";
                                $Listato = CreaLista($Variabile);
                                $TestoFinale .= CreaCheckbox($CampoModifica,$Listato,$Valore,$labells);
                                $TestoFinale .= "</div>";
                                //echo $Variabile;
                        }  else if (ereg("^select", $tipoCampo)) {
                                $Variabile=substr($tipoCampo, 7, strlen($tipoCampo)-8);
                                $TestoFinale .= "<label class=\"col-sm-2 control-label\">".$labells."</label>";
				$TestoFinale .= "<div class=\"col-sm-".$Largh."\">";
                                $TestoFinale .= CreaSelect($CampoModifica,$Variabile,$Valore,"class=\"form-control\"");
                                $TestoFinale .= "</div>";
                                //echo $Variabile;
                        }  else if (ereg("^adesso", $tipoCampo)) {
                        $TestoFinale .= "<div class=\"form-group\">";
                        $TestoFinale .= '<input class="form-control input-sm" id="exampleI" placeholder="'.$labells.'" type="text" style="width: 80px;" maxlength="10" value="'.date("d/m/Y").'" name="'.$CampoModifica.'"/>';
                        $TestoFinale .= '<script language="javascript">new vlaDatePicker("'.$CampoModifica.'");</script>';
                        $TestoFinale .= '</div>';
                        } else if (ereg("^Date", $tipoCampo)) {
                        $TestoFinale .= "<div class=\"form-group\">";
                        $TestoFinale .= '<input class="form-control input-sm" id="'.$CampoModifica.'" placeholder="'.$labells.'" type="text" style="width: 80px;" maxlength="10" value="'.MyHuman($Valore).'" name="'.$CampoModifica.'"/>';
                        $TestoFinale .= '<script language="javascript">new vlaDatePicker("'.$CampoModifica.'",{ prefillDate: false });</script>';
                        $TestoFinale .= '</div>';
                        } else if (ereg("^password", $tipoCampo)) {
                                if ($Valore!="") {
                                $InPassDaPass = $Valore;
                                } else {
                                $InPassDaPass = rand(10000000,99999999);
                                }
                        echo '<td><input type=password name="'.$CampoModifica.'" value="'.$InPassDaPass.'"></td>';
                        } else if (ereg("^hidden", $tipoCampo)) {
                        echo '<td><input type=hidden name="'.$CampoModifica.'" value="'.$Valore.'"></td>';
                        } else if (ereg("^Progressivo", $tipoCampo)) {
                        echo "<td>".$labells."</td>";
                        echo '<td><input name="'.$CampoModifica.'" value="'.$Valore.'" readonly></td>';
                        } else if (ereg("fckeditor", $tipoCampo)) {
                        echo "<td>".$labells."</td>";
                        echo '<td>';
                                        $sBasePath = $_SERVER['PHP_SELF'] ;
                                        $sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "worldnet/fckeditor" ) ) ;
                                        $oFCKeditor = new FCKeditor($CampoModifica) ;
                                        $oFCKeditor->BasePath   = $sBasePath ;
                                        $oFCKeditor->Value              = $Valore ;
                                        $oFCKeditor->Width  = '100%' ;
                                        $oFCKeditor->Height = '400' ;
                                        $oFCKeditor->Create() ;
                        echo "</td>";
                        } elseif (ereg("area", $tipoCampo)) {
                        $Numeri=explode("area", $tipoCampo);
                        $TestoFinale .= "<label class=\"col-sm-2 control-label\">".$labells."</label>";
                        $TestoFinale .= "<div class=\"col-sm-".$Largh."\">";
                        $TestoFinale .= '<textarea name="'.$CampoModifica.'" rows="5" '.$txtAdd.' class="form-control">'.$Valore.'</textarea>';
                        $TestoFinale .= "</div>";
                        } else {
			$TestoFinale .= "<label class=\"col-sm-2 control-label\">".$labells."</label>";
			$TestoFinale .= "<div class=\"col-sm-".$Largh."\">";
                        $TestoFinale .= '<input type=text class="form-control input-sm" size="'.$tipoCampo.'" name="'.$CampoModifica.'" value="'.$Valore.'" '.$txtAdd.' placeholder="'.$labells.'">';
			$TestoFinale .= "</div>";
                        }
			echo $TestoFinale;
			if ( ($Conta + $LarghT ) == 12) {
				echo "</div>";
				return 0;				
			} else {
				return ($Conta + $LarghT);
			}
}

Function CreaPuntoDomanda ($tipoCampo,$CampoModifica,$Valore,$labells,$MassimaLunghezza){
			$TestoFinale = "";
			if ($MassimaLunghezza>0) {
				$txtAdd = "onblur=\"javascript:VerificaCaratteri(this,".$MassimaLunghezza.")\"";
			} else {
				$txtAdd = "";
			}
			if (ereg("^selecampo", $tipoCampo)) {
				$Variabile=substr($tipoCampo, 10, strlen($tipoCampo)-11);
				$TestoFinale .= "<div class=\"form-group\"><div class=\"input-group\"><div class=\"input-group-addon input-sm\">".$labells."</div>";
				$Listato = CreaLista($Variabile);
				$TestoFinale .= CreaSelect($CampoModifica,$Listato,$Valore,"class=\"form-control input-sm\"");
				$TestoFinale .= "</div></div>";
				//echo $Variabile;
			} else if (ereg("^select", $tipoCampo)) {
				$Variabile=substr($tipoCampo, 7, strlen($tipoCampo)-8);
				$TestoFinale .= "<div class=\"form-group\"><div class=\"input-group\"><div class=\"input-group-addon input-sm\">".$labells."</div>";
				$TestoFinale .= CreaSelect($CampoModifica,$Variabile,$Valore,"class=\"form-control input-sm\"");
				$TestoFinale .= "</div></div>";
				//echo $Variabile;
			}  else if (ereg("^adesso", $tipoCampo)) {
			$TestoFinale .= "<div class=\"form-group\">";
			$TestoFinale .= '<input class="form-control input-sm" id="exampleI" placeholder="'.$labells.'" type="text" style="width: 80px;" maxlength="10" value="'.date("d/m/Y").'" name="'.$CampoModifica.'"/>';
			$TestoFinale .= '<script language="javascript">new vlaDatePicker("'.$CampoModifica.'");</script>';
			$TestoFinale .= '</div>';
			} else if (ereg("^Date", $tipoCampo)) {
			$TestoFinale .= "<div class=\"form-group\">";
			$TestoFinale .= '<input class="form-control input-sm" id="'.$CampoModifica.'" placeholder="'.$labells.'" type="text" style="width: 80px;" maxlength="10" value="'.MyHuman($Valore).'" name="'.$CampoModifica.'"/>';
			$TestoFinale .= '<script language="javascript">new vlaDatePicker("'.$CampoModifica.'",{ prefillDate: false });</script>';
			$TestoFinale .= '</div>';
			} else if (ereg("^password", $tipoCampo)) {
				if ($Valore!="") {
				$InPassDaPass = $Valore;
				} else {
				$InPassDaPass = rand(10000000,99999999);
				}
			echo '<td><input type=password name="'.$CampoModifica.'" value="'.$InPassDaPass.'"></td>';
			} else if (ereg("^hidden", $tipoCampo)) {
			echo '<td><input type=hidden name="'.$CampoModifica.'" value="'.$Valore.'"></td>';
			} else if (ereg("^Progressivo", $tipoCampo)) {
			echo "<td>".$labells."</td>";
			echo '<td><input name="'.$CampoModifica.'" value="'.$Valore.'" readonly></td>';
			} else if (ereg("fckeditor", $tipoCampo)) {
			echo "<td>".$labells."</td>";
			echo '<td>';
					$sBasePath = $_SERVER['PHP_SELF'] ;
					$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "worldnet/fckeditor" ) ) ;
					$oFCKeditor = new FCKeditor($CampoModifica) ;
					$oFCKeditor->BasePath	= $sBasePath ;
					$oFCKeditor->Value		= $Valore ;
					$oFCKeditor->Width  = '100%' ;
					$oFCKeditor->Height = '400' ;
					$oFCKeditor->Create() ;		
			echo "</td>";
			} elseif (ereg("area", $tipoCampo)) {
			$Numeri=explode("area", $tipoCampo);
			echo "<td>".$labells."</td>";
			echo '<td><textarea name="'.$CampoModifica.'" cols="'.$Numeri[0].'" rows="'.$Numeri[1].'" '.$txtAdd.'>'.$Valore.'</textarea></td>';
			} else {
			$TestoFinale .= "<div class=\"form-group\">";
			$TestoFinale .= '<input type=text class="form-control input-sm" size="'.$tipoCampo.'" name="'.$CampoModifica.'" value="'.$Valore.'" '.$txtAdd.' placeholder="'.$labells.'">';
			$TestoFinale .= '</div>';
			}
			return $TestoFinale;
}

function CreaLista($Dati) {
	global $conni;
	$Separatore="";
	$ListaCreata="";
	$VettLista=explode("++",$Dati);
	$VettListadiLista = explode("|",$VettLista[2]);
                if ((Count($VettListadiLista>1)) and ($VettListadiLista[1]!="")) {
                        $SSql="Select * from " . $VettLista[0] . " order by " .$VettListadiLista[0] . ",".$VettListadiLista[1].";";
                } else {
                        $SSql="Select * from " . $VettLista[0] . " order by " . $VettLista[2] . ";";
                }
	
	//echo $SSql;
	$risultatt=mysqli_query($conni,$SSql);
	while ($RRow = mysqli_fetch_array($risultatt)) {
	$ListaCreata .= $Separatore;
	$Separatore = "++";
	$g="";
	$ListaCreata .= $RRow[$VettLista[1]];
	$ListaCreata .= $Separatore;
                if ((Count($VettListadiLista>1)) and ($VettListadiLista[1]!="")) {
						for ($i=0;$i<count($VettListadiLista);$i++) {
							$ListaCreata .= $g." ".$RRow[$VettListadiLista[$i]];
							$g = " - ";
						}
                        
                } else {
                        $ListaCreata .= $RRow[$VettListadiLista[0]] ;
                }
	}
	return $ListaCreata;
}

function FindDati($Dati,$Valore) {
	global $conni;
	$Separatore="";
	$ListaCreata="";
	$VettLista=explode("++",$Dati);
	$SSql="Select * from " . str_replace(":",",",$VettLista[0]) . " where " . $VettLista[1] . "='" . $Valore . "' ".$VettLista[3]." order by " . $VettLista[2] . ";";
	//echo $SSql;
	$risultatt=mysqli_query($conni,$SSql);
	while ($RRow = mysqli_fetch_array($risultatt)) {
		$Val=$RRow[$VettLista[2]];
	}
	return $Val;
}

	function CreaSelect($Campo,$Lista,$ValTrovato,$Classe) {
	$VettLista=explode("++",$Lista);
	$ritorno = "";
	$ritorno .= "<select name=$Campo ".$Classe.">";
	$ritorno .= "<option value=\"\"> Scegli ".$label."";
		for ($x=0;$x<count($VettLista);$x=$x+2){
			if ($ValTrovato==str_replace("----",",",$VettLista[$x])) {
				$Chekkato="Selected";
			} else {
				$Chekkato="";
			}
			$ritorno .= "<option value=\"".str_replace("----",",",$VettLista[$x])."\" ".$Chekkato.">".str_replace("----",",",$VettLista[$x+1])."&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		$ritorno .= "</select>";
		return $ritorno;
	}

        function CreaCheckbox($Campo,$Lista,$ValTrovato,$label) {
	$ritorno = "";
        $VettLista=explode("++",$Lista);
                for ($x=0;$x<count($VettLista);$x=$x+2){

			$Chekkato = "";
                        if (strpos("||".$ValTrovato."||","||".$VettLista[$x]."||")!==false) {
                        	$Chekkato = " checked";
                        }
                        $ritorno .= "<input name=\"".$Campo."[]\" type=\"checkbox\" value=\"".$VettLista[$x]."\" ".$Chekkato.">".$VettLista[$x+1]."&nbsp;&nbsp;&nbsp;&nbsp;";
                        //if ($Indice>0 and ($Indice%3)==0) {
                                $ritorno .= "<br>";
                        //}

                }
		return $ritorno;
        }
	
Function CreaDettagli ($tipoCampo,$CampoModifica,$Valore){
			if (ereg("^select", $tipoCampo)) {
				$Variabile=substr($tipoCampo, 7, strlen($tipoCampo)-8);
				echo "<td>";
				CreaDettagliSelect($CampoModifica,$Variabile,$Valore);
				echo "</td>";
				//echo $Variabile;
                        } else if (ereg("^checkbox", $tipoCampo)) {
                                $Variabile=substr($tipoCampo, 9, strlen($tipoCampo)-10);
                                echo "<td>";
                                CreaDettagliCheck($CampoModifica,$Variabile,$Valore);
                                echo "</td>";
                                //echo $Variabile;

			} else if (ereg("^hidden", $tipoCampo)) {
			//echo '<td>'.$Valore.'</td>';
			} elseif (strrpos($tipoCampo, "area")) {
			echo '<td>'.$Valore.'</td>';
			} else {
			echo '<td>'.$Valore.'</td>';
			}
}

		function CreaDettagliSelect($Campo,$Lista,$ValTrovato) {
	$VettLista=explode("++",$Lista);
		for ($x=0;$x<count($VettLista);$x=$x+2){
			if ($ValTrovato==$VettLista[$x]) {
				echo "".$VettLista[$x+1]."";
			} else {
				echo "";
			}
		}
	}

                function CreaDettagliCheck($Campo,$Lista,$ValTrovato) {
		global $conni;
		$ValTrovato = str_replace("||",",",$ValTrovato);
        	$VettLista=explode("++",$Lista);
		$sql = "select * from ".$VettLista[0]." and ".$VettLista[1]." in (".$ValTrovato.") ";
		$risultato = mysqli_query($conni,$sql);
		$g = "";
		while ($riga = mysqli_fetch_array($risultato)) {
			echo $g . $riga[$VettLista[2]] . "";
			$g = ", ";
		}
        }
	
Function CostruttoMail ($VettoreCampiGet,$VettoreLabelCampiGet,$VettoreGestioneCampiGet,$TipoContenuto) {
	$CampiGet = explode(",", $VettoreCampiGet);
	$LabelGet = explode(",", $VettoreLabelCampiGet);
	$GestioneGet = explode(",", $VettoreGestioneCampiGet);
		if ($TipoContenuto=="HTML") {
		$testo = "<HTML><HEAD></HEAD><BODY>";
		} else {
		$testo = "";
		}
		for ($x=0;$x<count($CampiGet);$x++) {
			if ($TipoContenuto=="HTML") {
				$testo .= "<B>".$LabelGet[$x]."</B>";
				$testo .= "<br>";
				$testo .= str_replace("\n","<br>",DettagliMail($GestioneGet[$x],$CampiGet[$x],$_REQUEST[$CampiGet[$x]],""));
				$testo .= "<br><br>";
			} else {
				$testo .= $LabelGet[$x];
				$testo .= "\n";
				$testo .= DettagliMail($GestioneGet[$x],$CampiGet[$x],$_REQUEST[$CampiGet[$x]],"");
				$testo .= "\n\n";
			}
		}
		if ($TipoContenuto=="HTML") {
		return $testo."</BODY></HTML>";
		} else {
		return $testo;
		}
	}

Function DettagliMail ($tipoCampo,$CampoModifica,$Valore,$labells){
			if (ereg("^selecampo", $tipoCampo)) {
				$Variabile=substr($tipoCampo, 10, strlen($tipoCampo)-11);
				$Listato = CreaLista($Variabile);
				$VarRetur = CreaSelectMail($CampoModifica,$Listato,$Valore);
				//echo $Variabile;
			} else if (ereg("^select", $tipoCampo)) {
				$Variabile=substr($tipoCampo, 7, strlen($tipoCampo)-8);
				$VarRetur = CreaSelectMail($CampoModifica,$Variabile,$Valore);
				//echo $Variabile;
			}
			else if (ereg("^hidden", $tipoCampo)) {
			//echo '<td><input type=hidden name="'.$CampoModifica.'" value="'.$Valore.'"></td>';
			} else {
			$VarRetur= $Valore;
			}
	return $VarRetur;
}

function CreaSelectMail($Campo,$Lista,$ValTrovato) {
	$VettLista=explode("++",$Lista);
		for ($x=0;$x<count($VettLista);$x=$x+2){
			if ($ValTrovato==$VettLista[$x]) {
				$ret = $VettLista[$x+1];
			}
		}
		return $ret;
	}

function ReplaceStringaInMezzo($Testo,$Apertura,$Chiusura,$Riga) {
        if (preg_match('/'.$Apertura.'/i', $Testo) != 0) {
                if ( preg_match('/'.$Apertura.'/i', $Testo) != preg_match('/'.$Chiusura.'/i', $Testo)) {
                        echo "Errore !!!!!!";
                } else {
			$NumeroOccorrenze = preg_match('/'.$Apertura.'/i', $Testo);
                        $stop = 0;
                        while ($NumeroOccorrenze > 0) {
                        $InizioRecupero = $stop;
                                if (strpos($Testo,$Apertura,$InizioRecupero) !== false) {
                                        $start = strpos($Testo,$Apertura,$InizioRecupero);
                                        $stop  = strpos($Testo,$Chiusura,$start);
                                        $Testo = substr($Testo,0,$start). "".$Riga[substr($Testo,$start+1,(($stop)-($start+1)))]."" . substr($Testo,($stop+1));
					$NumeroOccorrenze = $NumeroOccorrenze -1;
                                } else {
					$NumeroOccorrenze = 0;
				}
                        }
                }
        } else {
        echo "No Caratteri Escape";
        }
        return $Testo;
}


function CreaMotoreRicerca($conni,$tabella,$CampiModifica,$TipoCampi,$campoId,$id,$labelModifica,$pagina,$SubmitTxt) {
$VettCampiModifica=explode(",", $CampiModifica);
$VettTipoCampo=explode(",", $TipoCampi);
$VettlabelModifica=explode(",", $labelModifica);
if (count($VettCampiModifica)==count($VettTipoCampo) AND  count($VettCampiModifica)==count($VettCampiModifica) AND count($VettCampiModifica)>0 AND $labelModifica<>"") {
echo '
	<div class="panel panel-default">
		<div class="panel-body">
			<form action="'.$pagina.'" method=get><input type=hidden name="Action" value="chg"><input type=hidden name="fai" value="1">
		<div class="row">
';
$sql="Select * From ".$tabella." where ".$campoId."=".$id.";";
//echo $sql;
$Sinistro = "";
$Destro = "";
		for ($x=0;$x<count($VettCampiModifica);$x++){
			CreaPuntoDomanda ($VettTipoCampo[$x],$VettCampiModifica[$x],$_REQUEST[$VettCampiModifica[$x]],$VettlabelModifica[$x],"",4);
			if (($x%2)==1) {
				$Destro .= CreaPuntoDomanda ($VettTipoCampo[$x],$VettCampiModifica[$x],$_REQUEST[$VettCampiModifica[$x]],$VettlabelModifica[$x],"",4);
			} else {
				$Sinistro .= CreaPuntoDomanda ($VettTipoCampo[$x],$VettCampiModifica[$x],$_REQUEST[$VettCampiModifica[$x]],$VettlabelModifica[$x],"",4);
			}
		}
echo "<div class=\"col-md-4\">      
                                        ".$Sinistro."                                    
                                 </div>";
echo "<div class=\"col-md-4\">      
                                        ".$Destro."                              
                                 </div>";
echo "<div class=\"col-md-4\">      
                                        <button type=\"submit\" class=\"btn btn-info form-control btn-sm\">Applica</button>                                    
                                 </div>";

echo "<input type=hidden name=Ordine value=\"".$_REQUEST["Ordine"]."\"><input type=hidden name=IdRiferimento value=".$id."><input type=hidden name=secondary value=\"".$_REQUEST["secondary"]."\"></div></form></div></div>";
}
}

function GestisciWhereMotore() {
	echo " ";
	
$StrCmp = "";
$StrVal = "";
$StrTip = "";
$StrSeg = "";
	

}

function VerificaClausoraWherePerCostrutto($StringaCampi,$StringaValori,$StringaTipi,$StringaSeguire) {
	$Campi = explode ("++",$StringaCampi);
	$Valori = explode ("++",$StringaValori);
	$Tipi = explode ("++",$StringaTipi);
	$Seguire = explode ("++",$StringaSeguire);
	$TempClausola = "";
		
		for ($Count=0 , $Rip=count($Campi);$Count<=$Rip;$Count++) {
		
			if ($Valori[$Count]<>"" AND $Valori[$Count]!=",") {
			 
				if (ereg("^TextStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."='" . $Valori[$Count] . "'"; 
				} else if (ereg("^NumStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."=" . $Valori[$Count] . "";
				} else if (ereg("^Mag",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." > '" . $Valori[$Count] . "'";
				} else if (ereg("^TextLike",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." like '%" . $Valori[$Count] . "%'";
				}  else if (ereg("^TextEreg",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." REGEXP '" . $Valori[$Count] . "'";
				}  else if (ereg("^RealExp",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." " . $Valori[$Count] . "";
				} else if (ereg("^DaA",$Tipi[$Count])) {
				 if ($Valori[$Count]!=",") {
				 $VettDate = explode(",",$Valori[$Count]);
				  if ($VettDate[0]!="" and $VettDate[1]!="") {
				  $TempClausola .= " ( " . $Campi[$Count] ." >" . $VettDate[0] . " AND " . $Campi[$Count] ." <" . $VettDate[1] . " ) ";
				  }
				 }
				} else {
				$XpTrGtHy = 0 ;
				}
			$TempClausola .= " " . $Seguire[$Count]  . " ";
			
			}	
		
		}
return $TempClausola;
}

function MyHuman($txt) {
		$DataHuman = substr($txt,8,2)."/".substr($txt,5,2)."/".substr($txt,0,4);
		if ($DataHuman!="00/00/0000") {
		return $DataHuman;
		} else {
		return "&nbsp;";
		}
}

function MyHumanConOra($txt) {
                $DataHuman = substr($txt,8,2)."/".substr($txt,5,2)."/".substr($txt,0,4);
                if ($DataHuman!="00/00/0000") {
                return $DataHuman . " " . substr($txt,11);;
                } else {
                return "&nbsp;";
                }
}

function FatturaMyHuman($txt) {
        $DataHuman = substr($txt,6,2)."/".substr($txt,4,2)."/".substr($txt,0,4);
        return $DataHuman;
}


function HumanMy($Data) {
        return substr($Data,6,4) . substr($Data,3,2) . substr($Data,0,2);
}
?>
