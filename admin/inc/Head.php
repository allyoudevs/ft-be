<head>
<link href="common/css/style.php" media="screen" type="text/css" rel="stylesheet">
<link href="common/css/style_print.php" media="print" type="text/css" rel="stylesheet">
<script src="script.js" type="text/javascript" language="javascript"></script>
<script src="ListiniSelect.php" type="text/javascript" language="javascript"></script>

	<link type="text/css" href="common/css/base/jquery-ui-1.8.custom.css" rel="stylesheet" />

	<script type="text/javascript" src="common/js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.core.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.blind.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.bounce.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.clip.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.drop.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.fold.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.effects.slide.js"></script>
	<script type="text/javascript" src="common/js/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="common/js/ui/i18n/jquery.ui.datepicker-it.js"></script>

	<title> Volaremedios Conference </title>
</head>
