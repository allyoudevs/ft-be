<?php

function VerificaPrivilegi($Reparto,$RepartiAutorizzati,$Loggato,$From) {
// Verifico se ho passato una lista di utenti autorizzati altrimenti lo sono tutti quelli che sono loggati
	if ($RepartiAutorizzati!="All") {
	//C'� una lista di autorizzazioni verifico l'utente autorizzato o lo rispedisco alla home
	$RepartiAutorizzati = "," . $RepartiAutorizzati . ",";
	$pos = strpos($RepartiAutorizzati, $Reparto);
		if (!isset($From) or $From=="") {
		$From = "control.php?NoPerm=Yes";
		}
		if ($pos === false) {
			echo "<script language=javascript>document.location.href='".$From."';</script>";
		} else {
			echo "<!-- L'untente risulta Autorizzato Per La Sezione Visualizzata, Procedo -->";
		}
		
	} else {
	//Non C'� lista autorizzati e quindi verifico se utente loggato e lo rimando al login
	if (!isset($From)) {
	$From = "index.php?NoLogin=Yes";
	}
		if ($Loggato==1) {
		echo "<!-- L'untente risulta Loggato, Procedo -->";
		} else {
		echo "<script language=javascript>document.location.href='".$From."';</script>";
		}
	}
}

function CreaMenuLi($ListaLink,$ListaPagine) {
	if ($ListaLink!="" && $ListaPagine!="") {
	//Riduco i Listati separati da virgole in Vettori
		$VettLink   = explode(",", $ListaLink);
		$VettPagine = explode(",", $ListaPagine);
	//Verifico che i vettori abbiano uguale lunghezza
		if ( count($VettLink) == count($VettPagine) ) {
			for ($x=0;$x<count($VettLink);$x++){
				if (ereg("^Titolo", $VettLink[$x])) {
				echo "<h3>" . $VettPagine[$x] . "</h3>";
				} else if ($VettLink[$x]=="") {
				echo "<br>";
				} else {
				echo '<li><a href="' . $VettLink[$x] . '">' . $VettPagine[$x] . '</a></li>';
				}
			}
		} else {
		echo "Errore I Vettori non Sono di Uguale Lunghezza verificare il codice del programma.";
		}
	} else {
	echo "Lista Vuota";
	}
}

function CreaCombo($listaNomi,$listaValori,$valore) {
$nomi=explode(",",$listaNomi);
$valori=explode(",",$listaValori);
$h=0;
        for ($i=0;$i<count($nomi);$i++) {
        $selected="";
        if ($valore==$valori[$i] OR ($i==(count($nomi)-1) AND $h=0)) {
        $selected="Selected";
        $h=1;
		$Cli = $nomi[$i];
        }
        echo '<option value="'.$valori[$i].'" '.$selected.'>'.$nomi[$i].'';
        }
	return $Cli;
}

function ClausolaWhere($StringaCampi,$StringaValori,$StringaTipi,$StringaSeguire) {
	$Campi = explode ("++",$StringaCampi);
	$Valori = explode ("++",$StringaValori);
	$Tipi = explode ("++",$StringaTipi);
	$Seguire = explode ("++",$StringaSeguire);
	$TempClausola = "";
		
		for ($Count=0 , $Rip=count($Campi);$Count<=$Rip;$Count++) {
		
			if ($Valori[$Count]<>"" AND $Valori[$Count]!=",") {
			 
				if (ereg("^TextStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."='" . $Valori[$Count] . "'"; 
				} else if (ereg("^NumStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."=" . $Valori[$Count] . "";
				} else if (ereg("^Mag",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." > '" . $Valori[$Count] . "'";
				} else if (ereg("^TextLike",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." like '%" . $Valori[$Count] . "%'";
				}  else if (ereg("^TextEreg",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." REGEXP '" . $Valori[$Count] . "'";
				}  else if (ereg("^RealExp",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." " . $Valori[$Count] . "";
				} else if (ereg("^DaA",$Tipi[$Count])) {
				 if ($Valori[$Count]!=",") {
				 $VettDate = explode(",",$Valori[$Count]);
				  if ($VettDate[0]!="" and $VettDate[1]!="") {
				  $TempClausola .= " ( " . $Campi[$Count] ." >" . $VettDate[0] . " AND " . $Campi[$Count] ." <" . $VettDate[1] . " ) ";
				  }
				 }
				} else {
				$XpTrGtHy = 0 ;
				}
			$TempClausola .= " " . $Seguire[$Count]  . " ";
			
			}	
		
		}
return $TempClausola;
}

function ListaId($Dato,$VarLista) {
$xxx = "";
        if ($Dato<>"") {
                if (!stristr($VarLista, "," . $Dato . ",")) {
                $xxx = $Dato .",";
                }
        }
		echo $xxx;
        return $xxx;
}

function TimeDaMySqlaIt($Time) {
	return substr($Time,6,2) . "-" . substr($Time,4,2) . "-" . substr($Time,0,4);
}

function comma_format($num){ 
   $arrot = ceil(str_replace(",",".",$num));
   $number = sprintf("%01.2f", $num); 
   $number = str_replace(".",",",$number);
while (preg_match("!(-?\d+)(\d\d\d)!", $number)) 
{ 
   $number = preg_replace("!(-?\d+)(\d\d\d)!","$1.$2", $number); 
} 
   if ($arrot==0) {
   $Ret = $number; 
   } else {
   $Ret = "<b>".$number."</b>";
   }
   return $Ret;
}
?>