<?php
	/* If you want to comply with E_STRICT php standards set a default timezone
	 * You can find default timezones here: http://us2.php.net/manual/en/timezones.php
	 */
	//date_default_timezone_set('Europe/Amsterdam');
	
	$wdays_labels	= array("Lu", "Ma", "Me", "Gi", "Ve", "Sa", "Do");
	$month_labels	= array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
	$month_s_labels	= array("Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Sep", "Ott", "Nov", "Dic");
?>