<?


/*

Please keep the following lines if you will use this script. Thanks!
---------------------------------------------------------------------------
	CALENDAR SCRIPT
Developed by : Steven Rebello (stevenrebello@indiatimes.com)
Developed on : 15 September 2001
Description : Prints a calendar for specified month and year in HTML

---------------------------------------------------------------------------


 To use this calendar script, just add the following  function 
 

print_calendar($mon,$year);

 
 into your code and
 place the function call print_calendar($month,$year) where you want the calendar to be printed.
 The function get_month_as_array
 
 $month and $year are integers.
 For eg. the following will print calendar for December 2001.
   
 print_calendar(12,2001); 

 You can tweak the table properties as you like.
 I did not want to complicate the function call with table property parameters like bordercolor etc..
 
*/

//----------------- This function is to do the HTML header chore---------------------
setlocale(LC_TIME, 'it_IT');
$month = $_GET["month"];
$year = $_GET["year"];
$campo = $_GET["campo"];
function print_HTML_header($Campo)
	{
		echo 	"<HTML>\n<HEAD>\n<STYLE>\n".
			 "<!--\nTD{ FONT-FAMILY:arial; FONT-SIZE:20px; }\n-->\n</STYLE>".
			 "<TITLE>Calendar\n</TITLE>\n<script language=javascript>\n\nfunction Modifica(Valore) {\nwindow.opener.document.forms[0].".$Campo.".value=Valore;\nwindow.close();\n }\n</script>\n</HEAD>\n<BODY BACKGROUND='images/bg.gif'>\n\n";
	}
	

//----------------- This function is to do the HTML footer chore---------------------
function  print_HTML_footer()
	{
		echo "</BODY></HTML>";
	}
	


//----------------- This function prints calendar---------------------
function  print_calendar($mon,$year,$campo)
	{
		global $dates, $first_day, $start_day;
			
		$first_day = mktime(0,0,0,$mon,1,$year);
		$start_day = date("w",$first_day);
		$start_day = ($start_day == 0)? 7:$start_day;
		$start_day = $start_day-1;
		$res = getdate($first_day);
		$month_name = $res["month"];
		$no_days_in_month = date("t",$first_day);
		
		//If month's first day does not start with first Sunday, fill table cell with a space
		for ($i = 1; $i <= $start_day;$i++)
			$dates[1][$i] = " ";

		$row = 1;
		$col = $start_day+1;
		$num = 1;
		while($num<=31)
			{
				if ($num > $no_days_in_month)
					 break;
				else
					{
						$dates[$row][$col] = $num;
						if (($col + 1) > 7)
							{
								$row++;
								$col = 1;
							}
						else
							$col++;
						$num++;
					}//if-else
			}//while
		$mon_num = date("n",$first_day);
		$temp_yr = $next_yr = $prev_yr = $year;

		$prev = $mon_num - 1;
		$next = $mon_num + 1;

		//If January is currently displayed, month previous is December of previous year
		if ($mon_num == 1)
			{
				$prev_yr = $year - 1;
				$prev = 12;
			}
    
		//If December is currently displayed, month next is January of next year
		if ($mon_num == 12)
			{
				$next_yr = $year + 1;
				$next = 1;
			}

		echo "<DIV ALIGN='right'><TABLE BORDER=1 WIDTH=40% CELLSPACING=0 BORDERCOLOR='silver'>";

		echo 	"\n<TR ALIGN='center'><TD BGCOLOR='white'> ".
			 "<A HREF='calendar.php?month=$prev&year=$prev_yr&campo=$campo' STYLE=\"text-decoration: none\"><B><<</B></A> </TD>".
			 "<TD COLSPAN=5 BGCOLOR='#99CCFF'><B>".ucfirst(strftime('%B',$first_day))." ".$temp_yr."</B></TD>".
			 "<TD BGCOLOR='white'> ".
			 "<A HREF='calendar.php?month=$next&year=$next_yr&campo=$campo' STYLE=\"text-decoration: none\"><B>>></B></A> </TD></TR>";

		echo "\n<TR ALIGN='center'><TD><B>Lun</B></TD><TD><B>Mar</B></TD>";
		echo "<TD><B>Mer</B></TD><TD><B>Gio</B></TD><TD><B>Ven</B></TD><TD><B>Sab</B></TD><TD><B>Dom</B></TD></TR>";
		echo "<TR><TD COLSPAN=7> </TR><TR ALIGN='center'>";
				
		$end = ($start_day > 4)? 6:5;
		for ($row=1;$row<=$end;$row++)
			{
				for ($col=1;$col<=7;$col++)
					{
						if ($dates[$row][$col] == "")
						$dates[$row][$col] = " ";
						
						if (!strcmp($dates[$row][$col]," "))
							$count++;
						
						$t = $dates[$row][$col];	
						
						//If date is today, highlight it
						if (($t == date("j")) && ($mon == date("n")) && ($year == date("Y")))
							echo "\n<TD BGCOLOR='aqua'><a href=\"javascript:Modifica('".$year."".substr("0".$mon,-2,2)."".substr("0".$t,-2,2)."')\">".$t."</a></TD>";
						else
							//If the date is absent ie after 31, print space
							echo "\n<TD>".(($t == " " )? "&nbsp;" : "<a href=\"javascript:Modifica('".$year."".substr("0".$mon,-2,2)."".substr("0".$t,-2,2)."')\">".$t."</a>")."</a></TD>";
					}// for -col
				
				if (($row + 1) != ($end+1))
					echo "</TR>\n<TR ALIGN='center'>";
				else
					echo "</TR>";
			}// for - row
		echo "\n</TABLE><BR><BR></DIV>";
	}

	
	
	
//----------Main Loop-----------

print_HTML_header($campo);

//If $month is not present, set it to current month.
$month = (empty($month)) ? date("n") : $month;

//If $year is not present, set it to current year.
$year = (empty($year)) ? date("Y") : $year;

print_calendar($month,$year,$campo);

print_HTML_footer();

?>

