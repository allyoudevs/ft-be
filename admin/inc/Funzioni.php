<?php

function VerificaPrivilegi($Reparto,$RepartiAutorizzati,$Loggato,$From) {
// Verifico se ho passato una lista di utenti autorizzati altrimenti lo sono tutti quelli che sono loggati
	if ($RepartiAutorizzati!="All") {
	//C'� una lista di autorizzazioni verifico l'utente autorizzato o lo rispedisco alla home
	$RepartiAutorizzati = "," . $RepartiAutorizzati . ",";
	$pos = strpos($RepartiAutorizzati, $Reparto);
		if (!isset($From) or $From=="") {
		$From = "control.php?NoPerm=Yes";
		}
		if ($pos === false) {
			echo "<script language=javascript>document.location.href='".$From."';</script>";
		} else {
			echo "<!-- L'untente risulta Autorizzato Per La Sezione Visualizzata, Procedo -->";
		}
		
	} else {
	//Non C'� lista autorizzati e quindi verifico se utente loggato e lo rimando al login
	if (!isset($From) or $From=="") {
	$From = "index.php?NoLogin=Yes";
	}
		if ($Loggato==1) {
		echo "<!-- L'untente risulta Loggato, Procedo -->";
		} else {
		echo "<script language=javascript>document.location.href='".$From."';</script>";
		}
	}
}

function CreaMenuLi($ListaLink,$ListaPagine) {
	if ($ListaLink!="" && $ListaPagine!="") {
	//Riduco i Listati separati da virgole in Vettori
		$VettLink   = explode(",", $ListaLink);
		$VettPagine = explode(",", $ListaPagine);
	//Verifico che i vettori abbiano uguale lunghezza
		if ( count($VettLink) == count($VettPagine) ) {
			for ($x=0;$x<count($VettLink);$x++){
				if (ereg("^Titolo", $VettLink[$x])) {
				echo "<h3>" . $VettPagine[$x] . "</h3>";
				} else if ($VettLink[$x]=="") {
				echo "<br>";
				} else {
				echo '<li><a href="' . $VettLink[$x] . '">' . $VettPagine[$x] . '</a></li>';
				}
			}
		} else {
		echo "Errore I Vettori non Sono di Uguale Lunghezza verificare il codice del programma.";
		}
	} else {
	echo "Lista Vuota";
	}
}

function CreaCombo($listaNomi,$listaValori,$valore) {
$nomi=explode(",",$listaNomi);
$valori=explode(",",$listaValori);
$h=0;
        for ($i=0;$i<count($nomi);$i++) {
        $selected="";
        if ($valore==$valori[$i] OR ($i==(count($nomi)-1) AND $h=0)) {
        $selected="Selected";
        $h=1;
		$Cli = $nomi[$i];
        }
        echo '<option value="'.$valori[$i].'" '.$selected.'>'.$nomi[$i].'';
        }
	return $Cli;
}

function ClausolaWhere($StringaCampi,$StringaValori,$StringaTipi,$StringaSeguire) {
	$Campi = explode ("++",$StringaCampi);
	$Valori = explode ("++",$StringaValori);
	$Tipi = explode ("++",$StringaTipi);
	$Seguire = explode ("++",$StringaSeguire);
	$TempClausola = "";
		
		for ($Count=0 , $Rip=count($Campi);$Count<=$Rip;$Count++) {
		
			if ($Valori[$Count]<>"" AND $Valori[$Count]!=",") {
			 
				if (ereg("^TextStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."='" . $Valori[$Count] . "'"; 
				} else if (ereg("^NumStrict",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ."=" . $Valori[$Count] . "";
				} else if (ereg("^Mag",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." > '" . $Valori[$Count] . "'";
				} else if (ereg("^TextLike",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." like '%" . $Valori[$Count] . "%'";
				}  else if (ereg("^TextEreg",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." REGEXP '" . $Valori[$Count] . "'";
				}  else if (ereg("^RealExp",$Tipi[$Count])) {
				$TempClausola .= " " . $Campi[$Count] ." " . $Valori[$Count] . "";
				} else if (ereg("^DaA",$Tipi[$Count])) {
				 if ($Valori[$Count]!=",") {
				 $VettDate = explode(",",$Valori[$Count]);
				  if ($VettDate[0]!="" and $VettDate[1]!="") {
				  $TempClausola .= " ( " . $Campi[$Count] ." >" . $VettDate[0] . " AND " . $Campi[$Count] ." <" . $VettDate[1] . " ) ";
				  }
				 }
				} else {
				$XpTrGtHy = 0 ;
				}
			$TempClausola .= " " . $Seguire[$Count]  . " ";
			
			}	
		
		}
return $TempClausola;
}

function ListaId($Dato,$VarLista) {
$xxx = "";
        if ($Dato<>"") {
                if (!stristr($VarLista, "," . $Dato . ",")) {
                $xxx = $Dato .",";
                }
        }
		echo $xxx;
        return $xxx;
}

function TimeDaMySqlaIt($Time) {
	return substr($Time,6,2) . "-" . substr($Time,4,2) . "-" . substr($Time,0,4);
}

function comma_format($num){ 
   $arrot = ceil(str_replace(",",".",$num));
   $number = sprintf("%01.2f", $num); 
   $number = str_replace(".",",",$number);
while (preg_match("!(-?\d+)(\d\d\d)!", $number)) 
{ 
   $number = preg_replace("!(-?\d+)(\d\d\d)!","$1.$2", $number); 
} 
   if ($arrot==0) {
   $Ret = $number; 
   } else {
   $Ret = "<b>".$number."</b>";
   }
   return $Ret;
}
		
function NuovoId($conn,$Campo,$Anno) {
		$Sql = "Select UltimoUsato from Contatori where Anno=".$Anno." and Campo='".$Campo."'";
		$result = mysql_query($Sql,$conn);
		if ($row=mysql_fetch_array($result)) {
			$NewMax = $row["UltimoUsato"]+1;
			$Sql = "Update Contatori set UltimoUsato=UltimoUsato+1 where Anno=".$Anno." and Campo='".$Campo."'";
		} else {
			$NewMax = 1;
			$Sql = "Insert into Contatori (Id,Campo,Anno,UltimoUsato) values (null,'".$Campo."',".$Anno.",1)";
		}
	mysql_query($Sql,$conn);
	return $NewMax."/".substr($Anno,-2);
}

	
function SgamaDato($conn,$dato,$id,$tabella,$campoid) {
	$Sql = "Select * from ".$tabella." where ".$campoid."=".$id."";
	$risultato = mysql_query($Sql,$conn);
	if ($riga = mysql_fetch_array($risultato)) {
		$Valore = $riga[$dato];	
	} else {
		$Valore = "";
	}
	return $Valore;
}

function RicostruisciData($Data,$Ora) {
	$Orario = split(":",$Ora);
	$DataTempo = $Data . substr("0".$Orario[0],-2) . substr("0".$Orario[1],-2) ."00";
	return $DataTempo;
} 

function AnaBolla($conn,$Id,$Campo) {
	$SqlCess = "Select ".$Campo." from Clienti where IdCliente='".$Id."'";
		$risultato = mysql_query($SqlCess,$conn);
		if ($riga=mysql_fetch_array($risultato)) {
			echo "".$riga[$Campo]."";
		} else {
			echo "&nbsp;";
		}
}

function CalcolaPrezzoTle ($IdProdotto,$CodiceListino,$conn,$UsaPromo=1) {
	//$TmpArray = array( 1=> 'Ink-jet', 2=>'Toner Monocromatico', 3=>'Fotoconduttori', 4=>'Toner a Colori', 5=>'Nastri', 6=>'Varie', 7=>'Materiale di Consumo', 8=>'Developer', 9=>'Toner Laserfix', 10=>'Gel', 11=>'Cera');
	$sql = "Select *,UNIX_TIMESTAMP(DataPromoDa) As InizioPromo,UNIX_TIMESTAMP(DataPromoA) as FinePromo,UNIX_TIMESTAMP(NOW()) AS DataAdesso from Prodotti where id_prodotto='".$IdProdotto."'";
	//echo $sql;
	$result = mysql_query($sql,$conn);
	while ($row=mysql_fetch_array($result)) {
		if ($row["DataAdesso"]<$row["FinePromo"] and $row["DataAdesso"]>$row["InizioPromo"]) {
			if ($UsaPromo == 0) {
				$PrezzoBase = $row["PrezzoRivenditore"];
			} else {
				$PrezzoBase = $row["PrezzoPromo"];
			}
		} else {
			$PrezzoBase = $row["PrezzoRivenditore"];
		}
		if ($CodiceListino == 0 ){
	
			if ($row["Prod"]=="TLE") {
				echo "<nobr>TLE: &euro; ".comma_format($row["PrezzoTLE"])."</nobr><br><nobr>TLE2: &euro; ".comma_format($row["PrezzoTLE2"])."</nobr><br><nobr>TLER1: &euro; ".comma_format($row["PrezzoTLER1"])."</nobr><br><nobr>TLER2: &euro; ".comma_format($row["PrezzoTLER2"])."</nobr><br><nobr>TLER3: &euro; ".comma_format($row["PrezzoTLER3"])."</nobr>";
			} else {
				echo "&euro; ".comma_format($row["PrezzoRivenditore"]);
			}
	
		} else  {
		
			$sql = "select PrezzoTLE,if(RicarichiProduttore.Ricarico>0,RicarichiProduttore.Ricarico,if(EccezioniCategorie.Ricarico>0,EccezioniCategorie.Ricarico,RicaricoConsumabili)) as RicaricoConsumabili,if(RicarichiProduttore.Ricarico>0,RicarichiProduttore.Ricarico,if(EccezioniCategorie.Ricarico>0,EccezioniCategorie.Ricarico,RicaricoEsprinet)) as RicaricoEsprinet from GruppiSconto left join EccezioniCategorie on GruppiSconto.IdGruppo=EccezioniCategorie.IdGruppo and EccezioniCategorie.Categoria='".str_replace("'","\'",$row["DescFam"])."' left join RicarichiProduttore on RicarichiProduttore.IdEccezioni=EccezioniCategorie.IdEccezioni and RicarichiProduttore.Produttore='".str_replace("'","\'",$row["NomeCasaProd"])."' where GruppiSconto.IdGruppo=".$CodiceListino."";
			//echo $sql;
			$Risultato = mysql_query($sql);
			if ($riga = mysql_fetch_array($Risultato)) {
				if ($row["Prod"]=="TLE") { 
					echo "&euro; ".comma_format($row["Prezzo".$riga["PrezzoTLE"]])."";
				} else if ($row["Tipo"] == 'Consumabili' or $row["Tipo"] == 'C' or $row["Tipo"] == 'MATERIALE DI CONSUMO') {
					$Ricarico = (100+$riga["RicaricoConsumabili"])/100;
					echo "&euro; ".comma_format($PrezzoBase*$Ricarico);
				} else {
					$Ricarico = (100+$riga["RicaricoEsprinet"])/100;
					echo "&euro; ".comma_format($PrezzoBase*$Ricarico);
				}
			} else {
				echo "Listino non Trovato";
			}
		}
	}
}

function CalcolaPrezzoTleReturn ($IdProdotto,$CodiceListino,$conn,$UsaPromo=1) {
	//$TmpArray = array( 1=> 'Ink-jet', 2=>'Toner Monocromatico', 3=>'Fotoconduttori', 4=>'Toner a Colori', 5=>'Nastri', 6=>'Varie', 7=>'Materiale di Consumo', 8=>'Developer', 9=>'Toner Laserfix', 10=>'Gel', 11=>'Cera');
	$sql = "Select *,UNIX_TIMESTAMP(DataPromoDa) As InizioPromo,UNIX_TIMESTAMP(DataPromoA) as FinePromo,UNIX_TIMESTAMP(NOW()) AS DataAdesso from Prodotti where id_prodotto='".$IdProdotto."'";
	$result = mysql_query($sql,$conn);
	while ($row=mysql_fetch_array($result)) {
		if ($row["DataAdesso"]<$row["FinePromo"] and $row["DataAdesso"]>$row["InizioPromo"]) {
			if ($UsaPromo == 0) {
				$PrezzoBase = $row["PrezzoRivenditore"];
			} else {
				$PrezzoBase = $row["PrezzoPromo"];
			}
		} else {
			$PrezzoBase = $row["PrezzoRivenditore"];
		}
		if ($CodiceListino == 0 ){
	
			if ($row["Prod"]=="TLE") {
				echo "Prezzo TLE: &euro; ".comma_format($row["PrezzoTLE"])."Prezzo TLE2: &euro; ".comma_format($row["PrezzoTLE2"])."Prezzo TLER1: &euro; ".comma_format($row["PrezzoTLER1"])."";
			} else {
				echo "".comma_format($row["PrezzoRivenditore"]);
			}
	
		} else  {
		
			$sql = "select PrezzoTLE,if(RicarichiProduttore.Ricarico>0,RicarichiProduttore.Ricarico,if(EccezioniCategorie.Ricarico>0,EccezioniCategorie.Ricarico,RicaricoConsumabili)) as RicaricoConsumabili,if(RicarichiProduttore.Ricarico>0,RicarichiProduttore.Ricarico,if(EccezioniCategorie.Ricarico>0,EccezioniCategorie.Ricarico,RicaricoEsprinet)) as RicaricoEsprinet from GruppiSconto left join EccezioniCategorie on GruppiSconto.IdGruppo=EccezioniCategorie.IdGruppo and EccezioniCategorie.Categoria='".str_replace("'","\'",$row["DescFam"])."' left join RicarichiProduttore on RicarichiProduttore.IdEccezioni=EccezioniCategorie.IdEccezioni and RicarichiProduttore.Produttore='".str_replace("'","\'",$row["NomeCasaProd"])."' where GruppiSconto.IdGruppo=".$CodiceListino."";
			//echo $sql;
			$Risultato = mysql_query($sql);
			if ($riga = mysql_fetch_array($Risultato)) {
				if ($row["Prod"]=="TLE") { 
					return "".($row["Prezzo".$riga["PrezzoTLE"]])."";
				} else if ($row["Tipo"] == 'Consumabili' or $row["Tipo"] == 'C' or $row["Tipo"] == 'MATERIALE DI CONSUMO') {
					//echo $row["PrezzoRivenditore"];
					$Ricarico = (100+$riga["RicaricoConsumabili"])/100;
					return "".($PrezzoBase*$Ricarico);
				} else {
					//echo $row["PrezzoRivenditore"];
					$Ricarico = (100+$riga["RicaricoEsprinet"])/100;
					return "".($PrezzoBase*$Ricarico);
				}
			} else {
				return "Listino non Trovato";
			}
		}
	}
}

function VerificaDispo ($IdProdotto,$conn) {
	$sql = "Select * from Prodotti where id_prodotto='".$IdProdotto."'";
	$result = mysql_query($sql,$conn);

	if ($row = mysql_fetch_array($result)) {

		if ($row["Prod"]=="TLE") { 
                        switch($row["DISPO"]) {
                        	case 3:
                              	echo " <img src='../images/icons/TELEFONARE.jpg' width='70' height='19' align='absmiddle'/>";
                              	break;
                      		case 2:
                               	echo " <img src='../images/icons/DISPONIBILE.jpg'  width='70' height='19' align='absmiddle' />";
                               	break;
                        	case 1:
				echo " <img src='../images/icons/RICHIESTA VUOTI.jpg'  width='70' height='19' align='absmiddle' />";
                                break;
                        }
		} else {
			echo $row["DISPO"];
		}
	}
	
}

?>
