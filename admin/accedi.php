<?php
session_start();
include("inc/connessione.inc.php");

$stanza = $_REQUEST["meetingID"];

$attendee_password = "att".$stanza."!".$stanza."dee";
$moderator_password = $stanza . "mod!rat".$stanza;

$password_accesso = "";

date_default_timezone_set('UTC');
// Include the composer autoloader
$loader = require_once __DIR__.'/vendor/autoload.php';

use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;
use BigBlueButton\Responses\IsMeetingRunningResponse;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;

//PRIMA DI TUTTO BISOGNA CONTROLLARE SE L'UTENTE

$sql = "Select * from users where user like '".mysqli_real_escape_string($conni,$_REQUEST["username"])."' and pass like '".mysqli_real_escape_string($conni,$_REQUEST["password"])."' and idCliente=".$parametriDominio["idCliente"]." ";
$risultato = mysqli_query($conni,$sql);
if ($riga = mysqli_fetch_array($risultato)) {
	$userID = $riga["id"];
	if ($_REQUEST["meetingID"] == 0 and $riga["differita"] != 'SI') {
		header("location: ../index.php?errore=5");
		die();
	}
	if ($_REQUEST["meetingID"]>0) {
		//SE QUI NON SONO MORTO DEVO CONTROLLARE LA STANZA
		$sql = "select * from rooms where id = ".$_REQUEST["meetingID"]." ";
		$result = mysqli_query($conni,$sql);
		if ($row = mysqli_fetch_array($result)) {
			$stanzeUtente = explode("||",$riga["corsi"]);
			if ( ( $row["tipoStanza"] == 'riservata' and !in_array($_REQUEST["meetingID"],$stanzeUtente) ) or $riga["diretta"] == 'NO' ) {
				header("location: ../index.php?errore=3");
				die();
			}
			if ($riga["admin"] == 'SI') {
				$password_accesso = $moderator_password;
			} else {
				$password_accesso = $attendee_password;
			}
			$nomeUtente = $riga["nome_cognome"];
		} else {
       	 	header("location: ../index.php?errore=2");
       	 	die();
		}		
	}
} else {
	header("location: ../index.php?errore=1");
	die();
}

$_SESSION["userID"] = $userID;
$_SESSION["roomID"] = $stanza;

if ($_REQUEST["meetingID"] == 0) {
	header("location: ../differita.php");
}

$bbb = new BigBlueButton();

$attendee_password = "att".$stanza."!".$stanza."dee";
$moderator_password = $stanza . "mod!rat".$stanza;
$isMeetingRunning = 0;

	$meetingRunningParms = new IsMeetingRunningParameters($stanza);
	$result = $bbb->isMeetingRunning($meetingRunningParms);
	if ( $result->getReturnCode() == 'SUCCESS') {
		$xml = simplexml_load_string($result->getRawXml()->asXML());
		if ($xml->running == 'false') {
			$isMeetingRunning = 0;
		} else {
			$isMeetingRunning = 1;
		}
	} else {
		echo "SERVER NON RAGGIUNGIBILE";
	}

//se il meeting non stà girando va creato
if ($isMeetingRunning == 0) {

	$createMeetingParams = new CreateMeetingParameters($stanza, $meetingName);
	$createMeetingParams->setAttendeePassword($attendee_password);
	$createMeetingParams->setModeratorPassword($moderator_password);
	//$createMeetingParams->setDuration($duration);
	$createMeetingParams->setLogoutUrl($parametriDominio["protocollo"]."://".$parametriDominio["dominio"]);
	if ($isRecordingTrue) {
		$createMeetingParams->setRecord(true);
		$createMeetingParams->setAllowStartStopRecording(true);
		$createMeetingParams->setAutoStartRecording(true);
	}

	$response = $bbb->createMeeting($createMeetingParams);
	if ($response->getReturnCode() == 'FAILED') {
		die('Can\'t create room! please contact our administrator.');
	} else {
		// process after room created
	}

} else {
	//CONTROLLO L'UTENTE DUPLICATO
	$bbb = new BigBlueButton();

	$getMeetingInfoParams = new GetMeetingInfoParameters($stanza, '', $moderator_password);
	$response = $bbb->getMeetingInfo($getMeetingInfoParams);
	$xml = simplexml_load_string($response->getRawXml()->asXML());
	foreach ($xml->attendees->attendee as $attendee) {
		print_r($attendee);
		if ($attendee->fullName == $nomeUtente) {
			header("location: ../index.php?errore=4");	
			die();
		}
	}
}


// $moderator_password for moderator
$joinMeetingParams = new JoinMeetingParameters($stanza, $nomeUtente, $password_accesso);
$joinMeetingParams->setRedirect(true);
$url = $bbb->getJoinMeetingURL($joinMeetingParams);

header('Location:' . $url);

?>
