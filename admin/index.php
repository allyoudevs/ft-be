<?php
	session_start();
	@session_destroy();
	$_SESSION["Reparto"] = 0;
	$_SESSION["Loggato"] = 0;
?>
<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entra nella tua Aula Virtuale</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
body {
        background: url(image/bg-login.jpg);
        background-size:cover;
}
.vertical-offset-100 {
        padding-top:100px;
}
.img-responsive {
        margin: 0 auto 30px auto;
}
.panel {
        border-radius:0;
        padding:30px;
}
.btn-success {
        background-color: #1d9fab;
        border:0;
}
.btn-success:hover {
        background-color: #4fc9d4;
        border:0;
}
</style>
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="text-center"><img src="image/logo-auviol.png" class="img-responsive" /></p>
                    <form accept-charset="UTF-8" role="form" action="login.php">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Login" name="login" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <hr />
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Entra">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
