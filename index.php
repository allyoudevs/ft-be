<?php
session_start();
include("./admin/inc/connessione.inc.php");

if ($_SESSION["user_id"]>0 or $_SESSION["username"] == 'record') {
        $sql = "insert into logs (user_id,room_id,ora,action) values (".$_SESSION["user_id"].",".$_SESSION["room_id"].",NOW(),'Uscita')";
        mysqli_query($conni,$sql);

        if ($_SESSION["admin"] == 1) {
                $amministratore = 1;
                $room_id = $_SESSION["room_id"];
        }

        session_destroy();
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entra nella tua Aula Virtuale</title>
<link href="common/css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="common/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
function GoInFullscreen(element) {
	alert("A");
        if(element.requestFullscreen)
                element.requestFullscreen();
        else if(element.mozRequestFullScreen)
                element.mozRequestFullScreen();
        else if(element.webkitRequestFullscreen)
                element.webkitRequestFullscreen();
        else if(element.msRequestFullscreen)
                element.msRequestFullscreen();
}

        function checkPrev(theForm) {
                var room = $("#meetingID").val();
                $.ajax({
                    url: "get_pwd.php?room="+room+"",
                    type: 'GET',
                    async: false,
                    success: function(target_text){
                                //eval('ValPrec = '+Campo+';');
                                //alert(ValPrec);
                                if(target_text != ''){
                                        //alert(target_text);
                                        passwordstanza = target_text;
                                } else {
                                        passwordstanza = "InAssenzaDiQuelloCheNonCeNonSiPuoEntrare";
                                }
                    }
                });
                //alert(passwordstanza);
                var password = document.getElementById("password").value;

                        if (password==passwordstanza) {
                                var stato = true;
                        } else {
                          alert("password errata, verificare di disporre dei privilegi di accesso.");
                var stato = false;
                        }
                        
        return stato;
        }

        function ChiudiStanza(numero) {
                if (confirm("Gentile amministratore, vuoi procedere alla chiusura della stanza e alla generazione del file della differita ?")) { 
                        $.ajax({
                                 url: "admin/chiudi_diretta.php?id="+numero+"",
                                type: 'GET',
                                async: false,
                                success: function(target_text){
                                        //eval('ValPrec = '+Campo+';');
                                        //alert(ValPrec);
                                        alert(target_text);
                                }
                        });

                }
        }


        function ModificaAjax(locazione) {
                anewwindow = window.open (locazione, "AjWin", "toolbar=no, resizable=yes, scrollbars=yes, location=no, width="+800+", height="+550+", left="+((screen.width-800)/2)+", top="+((screen.height-550)/2)+"")
                anewwindow.creator=self
                anewwindow.focus();
        }

	function autenticaStanze() {
		var utente = $("#username").val();
		var password = $("#password").val();
		$('#meetingID').find('option').remove().end();

        	$.ajax({url: "lista_ajax.php?password="+password+"&username="+utente+"",
                	type: 'GET',
                	async: true,
                	timeout: 100,
                	success: function(target_text){
			var autenticato = splitCodes=target_text.split('|');
			if (autenticato[0] == 1) {
        			var splitCodes=autenticato[1].split('+++');
         			for(var i=0;i<splitCodes.length;i=i+2){
                			$('#meetingID').append($('<option></option>').val(splitCodes[i]).html(splitCodes[i+1]));
        			}                        
				$("#row_meetingID").show();
			} else {
				$("#row_meetingID").hide();
			}

			}
                }
        );

	}
        </script>       
    
    <script type="text/javascript" charset="utf-8" src="common/js/jquery.js"></script>

<style type="text/css">
body {
	background: url(common/image/<?= $parametriDominio["sfondo"] ?>);
	background-size:cover;
}
.vertical-offset-100 {
	padding-top:100px;
}
.img-responsive {
	margin: 0 auto 30px auto;
}
.panel {
	border-radius:0;
	padding:30px;
}
.btn-success {
	background-color: #1d9fab;
	border:0;
}
.btn-success:hover {
	background-color: #4fc9d4;
	border:0;
}
</style>
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p class="text-center"><img src="common/image/<?= $parametriDominio["logo"] ?>" class="img-responsive" /></p>
                    <!-- <FORM NAME="form1" METHOD="GET" action="api/gestione.php"> -->
			<FORM NAME="form1" METHOD="POST" action="admin/accedi.php">
			<fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="username" name="username" id="username" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" id="password" value="">
                            </div>
                            <hr />
                            <div class="form-group" id ="row_meetingID">
                                <select name="meetingID" id="meetingID" class="form-control">
			         <?php

			                $sql = "select * from rooms where stato='attiva' and id>0 and idCliente = ".$parametriDominio["idCliente"] ."";
			                $risultato = mysql_query($sql,$conn);
			                while ($riga = mysql_fetch_array($risultato)) {
			                        echo "<option value=\"".$riga["id"]."\">".$riga["name"]."</option>";
			                }


			        ?>
 				<option value="0">Differita</option> 
				</select>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Entra">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
-->
<?php
        if ($_REQUEST["errore"]==1) {
                echo "<script language=\"javascript\">alert(\"Utente e Password non accettati per la stanza richiesta.\");</script>";
        }

        if ($_REQUEST["errore"]==2) {
                echo "<script language=\"javascript\">alert(\"Stanza non rilevata.\");</script>";
        }

        if ($_REQUEST["errore"]==3) {
                echo "<script language=\"javascript\">alert(\"Il livello di autorizzazione non coincide con quello necessario per accedere a questa stanza.\");</script>";
        }

        if ($_REQUEST["errore"]==4) {
                echo "<script language=\"javascript\">alert(\"Utente duplicato\");</script>";
        }

        if ($_REQUEST["errore"]==5) {
                echo "<script language=\"javascript\">alert(\"Non si è autorizzati ad accedere alla differita.\");</script>";
        }

        if ($_REQUEST["errore"]==9) {
                echo "<script language=\"javascript\">alert(\"Nessuna stanza selezionata, si prega di inserire utente e password corretti e poi selezionare la stanza desiderata prima di procedere.\");</script>";
        }

        if ($amministratore == 1) {
                echo "<script language=\"javascript\">ChiudiStanza(".$room_id.");</script>";
        }
?>
	<script language="javascript">
	//	$("#row_meetingID").hide();
/*		$("#username").change(function() {
  			autenticaStanze();
		});
                $("#password").change(function() {
                        autenticaStanze();
                });
*/
	</script>
</body>
</html>
