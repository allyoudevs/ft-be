<?php
$loader = require_once __DIR__.'/admin/vendor/autoload.php';

use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\GetRecordingsParameters;

session_start();
if ($_SESSION["userID"] > 0) {
include("./admin/inc/connessione.inc.php");
?>
<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entra nella tua Aula Virtuale</title>
<link href="common/css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="common/js/jquery-1.7.1.js"></script>
<style type="text/css">
body {
	background: url(common/image/<?= $parametriDominio["sfondo"] ?>);
	background-size:cover;
}
.vertical-offset-100 {
	padding-top:100px;
}
.img-responsive {
	margin: 0 auto 30px auto;
}
.panel {
	border-radius:0;
	padding:30px;
}
.btn-success {
	background-color: #1d9fab;
	border:0;
}
.btn-success:hover {
	background-color: #4fc9d4;
	border:0;
}
</style>
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
         	<p class="text-center"><img src="common/image/<?= $parametriDominio["logo"] ?>" class="img-responsive" /></p>
		<?php
				$sql = "select * from users where id = ".$_SESSION["userID"]." ";
				$risultato = mysqli_fetch_array($conni,$sql);
				if ($riga = mysqli_fetch_array($risultato)) {
					$listaStanze = $riga["corsi"];
				}
				if ($listaStanze == '') {
					$listaStanze = 0;
				}
                                $sql = "select * from rooms where stato='attiva' and id>0 and ( ( idCliente = ".$parametriDominio["idCliente"] ." and tipoStanza like 'libera') or id in (".str_replace("||",",",$listaStanze).") ) ";
				//$sql = "select * from rooms where id>0 and id in (".$ListaLivelli.") and nascondi_differita not like 'si' order by id asc;";
                                $risultato = mysqli_query($conni,$sql);
				$conta = 0;
                                while ($riga = mysqli_fetch_array($risultato)) {

			                $recordingParams = new GetRecordingsParameters();
//					$recordingParams->setMeetingId($row["id"]);
			                $bbb = new BigBlueButton();
			                $response = $bbb->getRecordings($recordingParams);

			                if ($response->getReturnCode() == 'SUCCESS') {
			                        foreach ($response->getRawXml()->recordings->recording as $recording) {

                                                        echo "<p style=\"text-align: center\">Stanza: " . $riga["name"] . "<br>";
                                                        echo "Reg. Iniziata il: " . date("d/m/Y H:i",ceil($recording->startTime/1000)+18000) . "<br>";
                                                        echo "Reg. Terrminata il: " . date("d/m/Y H:i",ceil($recording->endTime/1000)+18000) . "<br>";
                                                        echo "<a href=\"".$recording->playback->format->url."\" target=new>Guarda Differita</a>".$TestoVideo."</p>";
							$conta = $conta + 1;

						}
                                        }
				}
			if ($conta == 0) {
				echo "Nessuna differita disponibile";
			}
                        ?>

                <p align="center"><a href="index.php">Esci</a></p>
		</div>
            </div>
        </div>
    </div>
</div>
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
-->
</body>
</html>
<?php } else {
        header("location: index.php?errore=1");
        die();	
} ?>
